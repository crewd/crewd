using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Crewd.Persistence;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace Crewd.WebSite.Utils 
{
    public class Helpers
    {
        public static Boolean IsLoggedIn (HttpContext context)
        {
            byte[] token;
            if (!context.Session.TryGetValue("auth_token", out token))
            {
                return false;
            }
            return true;
        }

        public static async Task<Boolean> UserHasRole (HttpClient client, HttpContext context, String role)
        {            
            byte[] token;
            if (!context.Session.TryGetValue("auth_token", out token))
            {
                return false;
            }

            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.GetAsync("api/usertype/" + Auth);

            if(Response.IsSuccessStatusCode)
            {
                String Type = await Response.Content.ReadAsStringAsync();

                if(Type.Equals(role))
                {
                    return true;
                }
            }

            return false;
        }
    }
}