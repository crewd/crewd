﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crewd.WebSite.Models;
using Crewd.Persistence;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http.Headers;
using System.Text;
using System.Net;

namespace Crewd.WebSite.Controllers
{
    public class CoursesController : Controller
    {
        private static HttpClient client;
        private static readonly string baseAdress = "http://api:5000/";

        public CoursesController()
        {
          
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.UseCookies = true;
            //clientHandler.CookieContainer = new CookieContainer();
            clientHandler.UseDefaultCredentials = true;

            client = new HttpClient(clientHandler);
            client.BaseAddress = new Uri(baseAdress);
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("text/plain");
            client.DefaultRequestHeaders.Accept.Add(contentType);
        }

        // Return the Add Course page (For teachers only!)
        public async Task<IActionResult> Add()
        {
            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.GetAsync("api/usertype/" + Auth);

            if(Response.IsSuccessStatusCode)
            {
                String Type = await Response.Content.ReadAsStringAsync();

                if(Type.Equals("teacher"))
                {
                    CourseViewModel VM = new CourseViewModel();
                    ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                    ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
                    return View(VM);
                }
            }
           
            return RedirectToAction("List");
        }

        // Return new partial view with new question row to ajax call
        [HttpGet]
        public IActionResult AddQuestion()
        {
            var Skill = new SkillViewModel();       
            return PartialView("AddSkill", Skill);
        }

        // Add new Course (For teachers only!)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(CourseViewModel Course)
        {
            if(ModelState.IsValid)
            {
                byte[] token;
                if (!HttpContext.Session.TryGetValue("auth_token", out token))
                {
                    return RedirectToAction("List");
                }
                Course.Auth = Encoding.ASCII.GetString(token);

                Console.WriteLine("posting course, nr of skills: " + Course.Skills.Count());
                Console.WriteLine(Course.Skills[0].Name);
                var Response = await client.PostAsJsonAsync("api/courses/", Course);

                if (Response.IsSuccessStatusCode)
                {
                    return RedirectToAction("List");
                }

            }

            return RedirectToAction("List");
        }

        // Return the page with the list of all courses
        public async Task<IActionResult> List()
        {
            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.GetAsync("api/courses/" + Auth);
            ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
            ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");

            if (Response.IsSuccessStatusCode)
            {
                IEnumerable<Course> Courses = JsonConvert.DeserializeObject<IEnumerable<Course>>(await Response.Content.ReadAsStringAsync());
                return View(Courses.ToList());
            }
        
            Console.WriteLine("error getting courses :" + Response.StatusCode); 
            return View();
        }
        
        // Get the details of the course
        public async Task<IActionResult> Details(String CourseId)
        {
            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.GetAsync("api/courses/details/" + CourseId + "/" + Auth);

            if(Response.IsSuccessStatusCode)
            {
                DetailsViewModel Details = JsonConvert.DeserializeObject<DetailsViewModel>(await Response.Content.ReadAsStringAsync());
                ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
                return View("Details", Details);
            }

            return RedirectToAction("List");
        }

           

        // Close course registration (only permitted for the teacher who created the course)
        public async Task<IActionResult> Close(String CourseId)
        {
            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.PutAsync("api/courses/close/" + CourseId + "/" + Auth, null);

            if (!Response.IsSuccessStatusCode)
            {
                String ErrorMessage = await Response.Content.ReadAsStringAsync();

                Console.WriteLine("Error while closing this course " + ErrorMessage);              
                TempData["ErrorMess"] = ErrorMessage;
                return RedirectToAction("List");
            }

            Console.WriteLine("Course closed");
            TempData["SuccMess"] = "Course was closed successfully!";
            return RedirectToAction("List");
        }

        // Remove course (only permitted for the teacher who created the course)
        public async Task<IActionResult> Remove(String CourseId)
        {
            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.DeleteAsync("api/courses/" + CourseId + "/" + Auth);

            if(!Response.IsSuccessStatusCode)
            {
                String ErrorMessage = await Response.Content.ReadAsStringAsync();

                Console.WriteLine("Error deleting course " + ErrorMessage);
                TempData["ErrorMess"] = ErrorMessage;
                return RedirectToAction("List");
            }

            Console.WriteLine("Course removed");
            TempData["SuccMess"] = "Course was removed successfully!";
            return RedirectToAction("List");
        }

     

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
