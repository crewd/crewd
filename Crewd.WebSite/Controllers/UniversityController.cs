﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crewd.Persistence;
using System.Net.Http;
using System.Text;
using System.Net;

namespace Crewd.WebSite.Controllers
{
    public class UniversityController : Controller
    {
        private static HttpClient client;
        private static readonly string baseAdress = "http://api:5000/";

        public UniversityController()
        { 
             client = new HttpClient()
             {
                BaseAddress = new Uri(baseAdress)
             };
        }

        public IActionResult Index()
        {
            return View("AddTeachers");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(AddTeacherViewModel TeacherData)
        {
            if (ModelState.IsValid)
            {
                //Console.WriteLine("username: "+user.UserName);
                byte[] token;
                if (!HttpContext.Session.TryGetValue("auth_token", out token))
                {
                    return RedirectToAction("Login", "Login");
                }
                TeacherData.Auth = Encoding.ASCII.GetString(token);
  
                var response = await client.PostAsJsonAsync("api/university/", TeacherData);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Login", "Login");
                }

            }

            return View("AddTeachers");
        }
    }
}