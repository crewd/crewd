﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Crewd.WebSite.Controllers
{
    public class TeamsController : Controller
    {
        private static HttpClient client;
        private static readonly string baseAdress = "http://api:5000/";

        public TeamsController()
        {
            client = new HttpClient()
            {
                BaseAddress = new Uri(baseAdress)
            };
        }

        // List Teams and free team slots
        public async Task<IActionResult> ListSlots(String CourseId)
        {
            HttpResponseMessage Response = await client.GetAsync("api/teams/" + CourseId);

            if (Response.IsSuccessStatusCode)
            {
                TeamListSlotsViewModel VM =
                    JsonConvert.DeserializeObject<TeamListSlotsViewModel>(await Response.Content.ReadAsStringAsync());
                
                ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
                return View("ListSlots", VM);
            }

            return RedirectToAction("List", "Courses");
        }

        // Get the questions for this course and redirect to the Team registration page
        public async Task<IActionResult> Add(String CourseId)
        {
            AddTeamViewModel VM = new AddTeamViewModel();

            HttpResponseMessage Response = await client.GetAsync("api/teams/skills/" + CourseId);
            ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
            ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");

            if (Response.IsSuccessStatusCode)
            {
                List<Skill> CourseSkills =
                    JsonConvert.DeserializeObject<List<Skill>>(await Response.Content.ReadAsStringAsync());

                VM = new AddTeamViewModel(CourseSkills, CourseId);
                return View("Add", VM);
            }

            Console.WriteLine("There was a problem retrieving questions, course Id: " + CourseId);
            return View("Add", VM);
        }

        // Submit this team to the API
        [HttpPost]
        public async Task<IActionResult> Add(AddTeamViewModel VM)
        {
            if (ModelState.IsValid)
            {
                var Response = await client.PostAsJsonAsync("api/teams/", VM);

                if (!Response.IsSuccessStatusCode)
                {
                    Console.WriteLine("There was a problem submitting this team.");
                }
            }          
            return RedirectToAction("ListSlots");
        }

        // Get the details of this Team
        public async Task<IActionResult> Details(String TeamId)
        {
            var Response = await client.GetAsync("api/teams/details/" + TeamId);

            if (Response.IsSuccessStatusCode)
            {
                ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
                String Details = await Response.Content.ReadAsStringAsync();
                return View("Details", Details);
            }

            Console.WriteLine("There was a problem querying team details.");
            return RedirectToAction("ListSlots");
        }

        // Start the Gale-Shapley algorithm on this course
        public async Task<IActionResult> StartMatching(String CourseId)
        {
            Console.WriteLine("CID:  " +CourseId);
            HttpResponseMessage Response = await client.GetAsync("api/matching/" + CourseId);

            Console.WriteLine(Response.IsSuccessStatusCode);
            if (Response.IsSuccessStatusCode)
            {
                Dictionary<string, List<string>> Teams =
                    JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(await Response.Content.ReadAsStringAsync());

                //Console.WriteLine(Teams.Count());
                foreach(KeyValuePair<string, List<string>> Pair in Teams)
                {
                    Console.WriteLine("Team id:" + Pair.Key);
                    foreach(string val in Pair.Value)
                    {
                        Console.WriteLine("User id: " + val);
                    }
                }
                ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");

                //return RedirectToAction("ListSlots", "Teams", CourseId);
                return View("TeamsList", Teams);
            }

            Console.WriteLine("StartMatching: there was an error");
            return RedirectToAction("ListSlots", "Teams", CourseId);
        }
        
        /*public IActionResult TeamsList(Dictionary<string, List<string>> Data)
        {
            return View(Data);
        }*/
    }
}