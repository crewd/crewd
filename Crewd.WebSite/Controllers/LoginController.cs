using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crewd.Persistence;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace Crewd.WebSite.Controllers
{
    public class LoginController : Controller
    {
        private static HttpClient client;
        private static CookieContainer cookieContainer;
        //private static readonly string baseAdress = "http://docker_api_1:5000/";
        private static readonly string baseAdress = "http://api:5000/";

        public LoginController()
        {
            cookieContainer = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler
            {
                CookieContainer = cookieContainer,
                UseCookies = true,
                UseDefaultCredentials = true
            };
            client = new HttpClient(handler)
            {
                BaseAddress = new Uri(baseAdress)
            };
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("text/plain");
            client.DefaultRequestHeaders.Accept.Add(contentType);
        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> Login()
        {
            ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ValidateLogin(LoginViewModel user)
        {
            if(ModelState.IsValid)
            {
                //Console.WriteLine("username: "+user.UserName);

                String [] authData = { user.UserName, user.Password };
                Console.WriteLine("password " + user.Password);

                var response = await client.PostAsJsonAsync("api/login/", authData);

                if (response.IsSuccessStatusCode)
                {
                    var resp = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("resp: " + resp);
                    HttpContext.Session.Set("auth_token", Encoding.ASCII.GetBytes(resp));
                    return RedirectToAction("List", "Courses");
                }
             
            }         
            return View("Login");
        }

        public IActionResult Registration()
        {
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            var response = await client.PostAsJsonAsync("api/logout/", "");

            if (response.IsSuccessStatusCode)
            {
                byte[] token;
                if (!HttpContext.Session.TryGetValue("auth_token", out token))
                {
                    return RedirectToAction("Login");
                }
                HttpContext.Session.Remove("auth_token");
            }

            return RedirectToAction("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegistrationPost(RegistrationViewModel user)
        {
            if (ModelState.IsValid)
            {
                var response = await client.PostAsJsonAsync("api/register/", user);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            return View("Registration");
        }
    }
}