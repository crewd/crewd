﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Crewd.WebSite.Controllers
{
    public class CourseApplicationsController : Controller
    {
        private static HttpClient client;
        private static readonly string baseAdress = "http://api:5000/";

        public CourseApplicationsController()
        {
            client = new HttpClient()
            {
                BaseAddress = new Uri(baseAdress)
            };
        }

        // Get the Skills belonging to the selected course and redirect to course application page
        public async Task<IActionResult> Join(String CourseId)
        {

            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List", "Courses");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.GetAsync("api/applications/skills/" + CourseId + "/" + Auth);

            if (Response.IsSuccessStatusCode)
            {
                IEnumerable<Skill> Skills = JsonConvert.DeserializeObject<IEnumerable<Skill>>(await Response.Content.ReadAsStringAsync());

                JoinViewModel VM = new JoinViewModel(Skills.ToList(), CourseId);
                ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
                return View(VM);
            }

            String ErrorMessage = await Response.Content.ReadAsStringAsync(); 
            Console.WriteLine("Error while getting join page: " + ErrorMessage);
            TempData["ErrorMess"] = ErrorMessage;
            return RedirectToAction("List", "Courses");
        }

        // Post the course application and quiz answers to the API
        [HttpPost]
        public async Task<IActionResult> SubmitApplication(JoinViewModel VM)
        {           
            Console.WriteLine("course :" + VM.CourseId);

            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List", "Courses");
            }
            VM.Auth = Encoding.ASCII.GetString(token);

            var Response = await client.PostAsJsonAsync("api/applications/", VM);

            return RedirectToAction("List", "Courses");
        }

        // Return the student application approval page (For teachers only!)
        public async Task<IActionResult> AddStudent()
        {
            byte[] token;
            if (!HttpContext.Session.TryGetValue("auth_token", out token))
            {
                return RedirectToAction("List", "Courses");
            }
            String Auth = Encoding.ASCII.GetString(token);

            HttpResponseMessage Response = await client.GetAsync("api/applications/" + Auth);

            if (Response.IsSuccessStatusCode)
            {
                IEnumerable<CourseUser> Applications = JsonConvert.DeserializeObject<IEnumerable<CourseUser>>(await Response.Content.ReadAsStringAsync());
                ViewBag.IsTeacher = await Utils.Helpers.UserHasRole(client, HttpContext, "teacher");
                ViewBag.IsUniversity = await Utils.Helpers.UserHasRole(client, HttpContext, "university");
                return View(Applications.ToList());
            }

            Console.WriteLine("error getting applications :" + Response.StatusCode);
            return RedirectToAction("List", "Courses");
        }

        // Reject course application, delete it from the db
        public async Task<IActionResult> RejectApplication(String Id)
        {
            HttpResponseMessage Response = await client.PutAsJsonAsync("api/applications/reject/" + Id, 0);

            return RedirectToAction("AddStudent");
        }

        // Approve course application, update entry in db
        public async Task<IActionResult> ApproveApplication(String Id)
        {
            HttpResponseMessage Response = await client.PutAsJsonAsync("api/applications/approve/" + Id, 0);

            return RedirectToAction("AddStudent");
        }
    }
}