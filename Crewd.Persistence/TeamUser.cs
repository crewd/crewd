using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class TeamUser
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        [ForeignKey("Team")]
        public String TeamId { get; set; }

        public Team Team { get; set; }

        [Required]
        [ForeignKey("User")]
        public String UserId { get; set; }

        public User User { get; set; }
    }
}
