using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class Course
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        public String Name { get; set; }

        [Required]
        public int TeamSize { get; set; }

        [Required]
        public Boolean Open { get; set; }

        [Required]
        public String CourseDetails { get; set; }

        public ICollection<SkillCourse> SkillCourses { get; set; }

        public ICollection<CourseTeam> CourseTeams { get; set; }

        public ICollection<CourseUser> CourseUsers { get; set; }
    }
}
