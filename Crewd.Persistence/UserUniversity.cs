using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class UserUniversity
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        [ForeignKey("User")]
        public String UserId { get; set; }

        public User User { get; set; }

        [Required]
        [ForeignKey("University")]
        public String UniversityId { get; set; }

        public University University { get; set; }

        [Required]
        public Boolean IsTeacher { get; set; }
    }
}
