using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class Skill
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        public String Name { get; set; }
        
        [Required]
        public SkillType Type { get; set; }

        [Required]
        public Int32 Weight { get; set; }

        public ICollection<UserSkill> UserSkills { get; set; }

        public ICollection<SkillValue> SkillValues { get; set; }

        public ICollection<SkillCourse> SkillCourses { get; set; }

        public ICollection<TeamSkill> SkillTeamSlots { get; set; }
    }

    public enum SkillType
    {
        Checkbox,
        Dropdown,
        RadioButton
    }
}
