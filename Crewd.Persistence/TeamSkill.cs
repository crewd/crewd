using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class TeamSkill
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        [ForeignKey("Skill")]
        public String SkillId { get; set; }

        public Skill Skill { get; set; }

        [Required]
        [ForeignKey("TeamSlot")]
        public String TeamId { get; set; }

        public Team Team { get; set; }

        [Required]
        public String Value { get; set; }
    }
}
