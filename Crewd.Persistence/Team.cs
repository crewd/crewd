using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class Team
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        public String Name { get; set; }
        
        [Required]
        public String Details { get; set; }
        
        [Required]
        [ForeignKey("Course")]
        public String CourseId { get; set; }

        public Course Course { get; set; }

        public ICollection<SkillCourse> SkillCourses { get; set; }

        public ICollection<TeamSkill> TeamSkills { get; set; }
    }
}
