using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Crewd.Persistence
{
    public  class CrewdDbContext : IdentityDbContext<User>
    {
        public CrewdDbContext(DbContextOptions<CrewdDbContext> options)
            : base(options)
        {
        }

        public CrewdDbContext(){}

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<CourseTeam> CourseTeams { get; set; }
        public virtual DbSet<CourseUser> CourseUsers { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<SkillCourse> SkillCourses { get; set; }
        public virtual DbSet<SkillValue> SkillValues { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamSkill> TeamSkills { get; set; }
        public virtual DbSet<TeamUser> TeamUsers { get; set; }
        public virtual DbSet<University> Universities { get; set; }
        public virtual DbSet<UserSkill> UserSkills { get; set; }
        public virtual DbSet<UserUniversity> UserUniversities { get; set; }

        public virtual List<Course> getCourses()
        {
            return this.Courses.ToList();
        }

        public virtual List<CourseUser> getCourseUsers()
        {
            return this.CourseUsers.Include(cu => cu.User).ToList();
        }

        public virtual List<UserSkill> getUserSkills()
        {
            return this.UserSkills.ToList();
        }

        public virtual List<Team> getTeams()
        {
            return this.Teams.ToList();
        }

        public virtual List<TeamSkill> getTeamSkills()
        {
            return this.TeamSkills.ToList();
        }

        public virtual List<SkillCourse> getSkillCourses()
        {
            return this.SkillCourses.ToList();
        }

        public virtual List<Skill> getSkills()
        {
            return this.Skills.ToList();
        }
    }
}
