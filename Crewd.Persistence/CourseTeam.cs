using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class CourseTeam
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        [ForeignKey("Course")]
        public String CourseId { get; set; }

        public Course Course { get; set; }

        //[Required]
        [ForeignKey("Team")]
        public String TeamId { get; set; }

        public Team Team { get; set; }
    }
}
