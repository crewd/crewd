using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class DbInitializer
    {
        public static void Initialize(CrewdDbContext context, UserManager<User> userManager = null)
        {
            context.Database.EnsureDeleted();

            context.Database.EnsureCreated();

            if (context.Universities.Any())
            {
                return;
            }

            IList<University> Universities = new List<University> {
                new University
                {
                    Name = "ELTE"
                },
                new University
                {
                    Name = "BME"
                },
            };

            foreach (University university in Universities)
                context.Universities.Add(university);

            if (userManager != null) {
                var adminUser = new User
                {
                    UserName = "admin",
                    FullName = "Admin",
                    Email = "admin@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = false
                };
                var adminPassword = "1234";

                var result1 = userManager.CreateAsync(adminUser, adminPassword).Result;

                var elte =
                    context
                        .Universities
                        .Where(o => o.Name.Equals("ELTE"))
                        .FirstOrDefault();

                var bme =
                    context
                        .Universities
                        .Where(o => o.Name.Equals("BME"))
                        .FirstOrDefault();

                var elteUni = new User
                {
                    UserName = "elte",
                    FullName = "ELTE University",
                    Email = "elte@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = true,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = elte.Id, IsTeacher = true } }
                };

                var resultElte = userManager.CreateAsync(elteUni, adminPassword).Result;

                var bmeUni = new User
                {
                    UserName = "bme",
                    FullName = "BME University",
                    Email = "bme@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = true,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = bme.Id, IsTeacher = true } }
                };

                var resultBme = userManager.CreateAsync(bmeUni, adminPassword).Result;

                var elteUser = new User
                {
                    UserName = "elteUser",
                    FullName = "ELTE User",
                    Email = "elte@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = false,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = elte.Id, IsTeacher = false } }
                };

                var resultElteUser = userManager.CreateAsync(elteUser, adminPassword).Result;

                var bmeUser = new User
                {
                    UserName = "bmeUser",
                    FullName = "BME User",
                    Email = "bme@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = false,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = bme.Id, IsTeacher = false } }
                };

                var resultBmeUser = userManager.CreateAsync(bmeUser, adminPassword).Result;

                var elteTeacher = new User
                {
                    UserName = "elteTeacher",
                    FullName = "ELTE Teacher",
                    Email = "elte@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = false,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = elte.Id, IsTeacher = true } }
                };

                var resultElteTeacher = userManager.CreateAsync(elteTeacher, adminPassword).Result;

                var bmeTeacher = new User
                {
                    UserName = "bmeTeacher",
                    FullName = "BME Teacher",
                    Email = "bme@example.com",
                    PhoneNumber = "+36123456789",
                    IsUniversity = false,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = bme.Id, IsTeacher = true } }
                };

                var resultBmeTeacher = userManager.CreateAsync(bmeTeacher, adminPassword).Result;
            }

            context.SaveChanges();
        }
    }
}
