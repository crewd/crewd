using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class SkillValue
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        [ForeignKey("Skill")]
        public String SkillId { get; set; }

        public Skill Skill { get; set; }

        [Required]
        public String Value { get; set; }
    }
}
