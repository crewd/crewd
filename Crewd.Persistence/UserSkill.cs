using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class UserSkill
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        [ForeignKey("User")]
        public String UserId { get; set; }

        public User User { get; set; }

        [Required]
        [ForeignKey("Skill")]
        public String SkillId { get; set; }

        public Skill Skill { get; set; }

        [Required]
        public String Value { get; set; }
    }
}
