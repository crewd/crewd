﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crewd.Persistence
{
    public class AddTeamViewModel
    {
        public String Name { get; set; }
        public String Details { get; set; }

        public String CourseId { get; set; }
        public IList<Question> Questions { get; set; }

        public String Auth { get; set; }

        public AddTeamViewModel(IEnumerable<Skill> Skills, String CourseId)
        {
            this.CourseId = CourseId;
            this.Questions = new List<Question>();

            foreach (Skill Skill in Skills)
            {
                Question Question = new Question(Skill);
                Questions.Add(Question);
            }
        }

        public AddTeamViewModel() { }
    }
    
}
