﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Crewd.Persistence
{
    public class RegistrationViewModel
    {
        [Required]
        public String FullName { get; set; }

        [Required]
        public String UserName { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public String Password { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public String ConfirmedPassword { get; set; }

        public String University { get; set; }

        public String Email { get; set; }

        //public File ProfilePicture { get; set; }

    }
}
