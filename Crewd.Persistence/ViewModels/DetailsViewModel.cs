﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crewd.Persistence
{
    public class DetailsViewModel
    {
        public String Details { get; set; }

        public Int32 NrOfStudents { get; set; }

        public String Teacher { get; set; }
    }
}
