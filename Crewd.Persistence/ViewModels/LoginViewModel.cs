using System;
using System.ComponentModel.DataAnnotations;  

namespace Crewd.Persistence
{
    public class LoginViewModel
    {
        [Required]
        public String UserName {get; set;}

        [DataType(DataType.Password)] 
        [Required] 
        public String Password {get; set;}
    }
}