﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crewd.Persistence
{
    public class TeamListSlotsViewModel
    {
        public String CourseId { get; set; }

        public Int32 NrOfTeams { get; set; }

        public IEnumerable<Team> Teams { get; set; }

    }
}
