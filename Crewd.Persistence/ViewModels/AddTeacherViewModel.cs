﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Crewd.Persistence
{
    public class AddTeacherViewModel
    {
        [Required]
        public String FullName { get; set; }

        [Required]
        public String UserName { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public String Password { get; set; }

        public String Auth { get; set; }

    }
}
