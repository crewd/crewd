﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crewd.Persistence
{
    public class JoinViewModel
    {
        public String CourseId { get; set; }
        public IList<Question> Questions { get; set; }
        public String Auth { get; set; }

        public JoinViewModel(IEnumerable<Skill> Skills, String CourseId)
        {
            this.CourseId = CourseId;
            this.Questions = new List<Question>();

            foreach(Skill Skill in Skills)
            {

                Question Question = new Question(Skill);
                Questions.Add(Question);
            }
        }

        public JoinViewModel() { }
    }

    public class Question
    {
        public String Id { get; set; }
        public String Text { get; set; }
        public SkillType Type { get; set; }

        public String SelectedValue { get; set; }
        public List<Boolean> CheckboxMultipleValues { get; set; }

        public IList<String> Values { get; set; }

        public Question(Skill Skill)
        {
            this.Text = Skill.Name;
            this.Type = Skill.Type;
            this.Id = Skill.Id;

            this.Values = new List<String>();            

            foreach(SkillValue SkillValue in Skill.SkillValues)
            {
                this.Values.Add(SkillValue.Value);
            }

            this.CheckboxMultipleValues = new List<Boolean>(new Boolean [this.Values.Count()]);
        }

        public Question() { }
    }

}
