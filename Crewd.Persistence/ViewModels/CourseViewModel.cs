﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Crewd.Persistence
{
    public class CourseViewModel
    {
        public String Auth { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public Int32 TeamSize { get; set; }

        [Required]
        public String CourseDetails { get; set; }

        public List<SkillViewModel> Skills { get; set; }

        public CourseViewModel()
        {
            Skills = new List<SkillViewModel>
            { 
                new SkillViewModel()
            };
        }
    }

    public class SkillViewModel
    {
        [Required]
        public String Name { get; set; }

        [Required]
        public SkillType Type { get; set; }

        [Required]
        [Range(1, 100)]
        public Int32 Weigth { get; set; }

        public List<String> Values { get; set; }

        public SkillViewModel()
        {
            Values = new List<String>(new string[5] );
        }
    }

}
