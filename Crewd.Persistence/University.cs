using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class University
    {
        [Key]
        public String Id { get; set; }
        
        [Required]
        public String Name { get; set; }

        public ICollection<UserUniversity> UserUniversities { get; set; }
    }
}
