using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Crewd.Persistence
{
    public class User : IdentityUser
    {
        [Required]
        public String FullName { get; set; }
        
        //[Required]
        public String ProfilePicture { get; set; }

        [Required]
        public Boolean IsUniversity { get; set; }

        public ICollection<UserUniversity> UserUniversities { get; set; }

        public ICollection<UserSkill> UserSkills { get; set; }

        public ICollection<CourseUser> CourseUsers { get; set; }

        public ICollection<TeamUser> TeamUsers { get; set; }
    }
}
