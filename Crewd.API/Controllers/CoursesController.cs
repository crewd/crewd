﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Crewd.API.Controllers
{
    [Route("api/")]
    public class CoursesController : ControllerBase
    {
        private readonly CrewdDbContext Context;

        public CoursesController(CrewdDbContext _context)
        {
            Context = _context;
        }

        // Get the list of courses
        [HttpGet]
        [Route("courses/{Auth}")]
        public IActionResult Get([FromRoute] String Auth)
        {
            var User = 
                Context
                    .Users
                    .Include(u => u.UserUniversities)
                    .Where(u => u.UserName.GetHashCode().ToString().Equals(Auth))
                    .FirstOrDefault();
            if (User != null)
            { 
                Console.WriteLine("authenticated");
                var courses =
                    Context
                        .Courses
                        .Include(c => c.CourseUsers)
                        .ThenInclude(c => c.User)
                        .ThenInclude(c => c.UserUniversities)
                        .Where(c => 
                            c.CourseUsers
                                .Where(u => 
                                    u.IsTeacher && 
                                    u.User.UserUniversities.Where(uu => 
                                        User.UserUniversities.Where(o => 
                                            o.UniversityId == uu.UniversityId && o.UniversityId != null
                                        ).Any()
                                    ).Any()
                                )
                                .Any()
                        )
                        .ToList();
                return Ok(courses);
            }

            Console.WriteLine("get lists of courses: failed to authenticate ");
            return StatusCode(StatusCodes.Status403Forbidden);
        }

        // Get the details of a specific course
        [HttpGet]
        [Route("courses/details/{CourseId}/{Auth}")]
        public IActionResult GetDetails([FromRoute] String CourseId, [FromRoute] String Auth)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Auth)).FirstOrDefault();
            if (User != null)
            {
                DetailsViewModel Data = new DetailsViewModel
                {
                    Details = Context.Courses.Find(CourseId).CourseDetails,
                    NrOfStudents = Context.CourseUsers.Where(x => x.IsApproved == true && x.IsTeacher == false).
                        Count(x => x.CourseId == CourseId),
                    Teacher = Context.CourseUsers.Where(x => x.CourseId == CourseId && x.IsTeacher == true).
                        Select(x => x.User).First().FullName
                };

                return Ok(Data);
            }

            return StatusCode(StatusCodes.Status403Forbidden);
        }

        // Add new Course
        [HttpPost]
        [Route("courses")]
        public IActionResult Add([FromBody] CourseViewModel Cvm)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Cvm.Auth)).FirstOrDefault();
            if (User != null)
            {
                Console.WriteLine("found user: " + User.UserName);
                Console.WriteLine("Nr of skills: " + Cvm.Skills.Count());

                // Add the course to the database (also add entry to CourseUsers table)
                Course Course = new Course()
                {
                    Name = Cvm.Name,
                    TeamSize = Cvm.TeamSize,
                    CourseDetails = Cvm.CourseDetails,
                    CourseUsers = new List<CourseUser> { new CourseUser { UserId = User.Id, IsTeacher = true} },
                    Open = true
                };
               
               
                for (Int32 i = 0; i < Cvm.Skills.Count(); ++i)
                {
                    if (!String.IsNullOrEmpty(Cvm.Skills[i].Name))
                    {
                        // Add the course skills to the database
                        Skill Skill = new Skill()
                        {
                            Name = Cvm.Skills[i].Name,
                            Type = Cvm.Skills[i].Type,
                            Weight = (Int32)Cvm.Skills[i].Weigth,
                            SkillValues = new List<SkillValue>()
                        };

                        // Add the skill-values of this skill to the db
                        foreach (String item in Cvm.Skills[i].Values)
                        {
                            if (!String.IsNullOrEmpty(item))
                                Skill.SkillValues.Add(new SkillValue() { Value = item });
                        }

                        // Connect this skill with the course
                        SkillCourse SK = new SkillCourse()
                        {
                            Skill = Skill,
                            Course = Course
                        };

                        Context.Add(Skill);
                        Context.Add(SK);
                    }
                }
                Context.Add(Course);
               
                Context.SaveChanges();

                Console.WriteLine("Course/Skills/SkillCourses/CourseUser were successfully added");

                return Ok();
            }
            Console.WriteLine("couldn't find user with hash: " + Cvm.Auth);
            return StatusCode(StatusCodes.Status403Forbidden);
        }
    
        // Close course registration
        // Authorized only if: user is the teacher who created the course.
        [HttpPut]
        [Route("courses/close/{CourseId}/{Auth}")]
        public IActionResult Close([FromRoute] String CourseId, [FromRoute] String Auth)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Auth)).FirstOrDefault();

            if (User != null)
            {           
                Course Course = Context.Courses.Find(CourseId);

                if (Course != null)
                {
                    CourseUser CourseUser = Context.CourseUsers.Where(cu => cu.UserId == User.Id && cu.CourseId == Course.Id).First();

                    if(CourseUser != null && CourseUser.IsTeacher == true)
                    {
                        Course.Open = false;
                        Context.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        var Response = new NotFoundObjectResult("Only the teacher of the course can close it!");
                        return Response;
                    }
                }              
            }

            return StatusCode(StatusCodes.Status403Forbidden);
        }

        // Delete course (only allowed to the teacher, who created it)
        [HttpDelete]
        [Route("courses/{CourseId}/{Auth}")]
        public IActionResult Remove([FromRoute] String CourseId, [FromRoute] String Auth)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Auth)).FirstOrDefault();
            Course Course = Context.Courses.Find(CourseId);

            if (User != null && Course != null)
            {
                CourseUser CourseUser = Context.CourseUsers.Where(cu => cu.UserId == User.Id && cu.CourseId == Course.Id).First();

                if (CourseUser != null && CourseUser.IsTeacher == true)
                {
                    Context.Remove(Course);
                    Context.SaveChanges();
                    return Ok();
                }
                else
                {
                    var Response = new NotFoundObjectResult("Only the teacher of the course can remove it!");
                    return Response;
                }
            }

            return StatusCode(StatusCodes.Status403Forbidden);
        }

    }
}