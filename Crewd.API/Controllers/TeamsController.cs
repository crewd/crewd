﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Crewd.API.Controllers
{
    [Route("api/")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly CrewdDbContext Context;

        public TeamsController(CrewdDbContext context)
        {
            Context = context;
        }
        

        [HttpGet]
        [Route("teams/{CourseId}")]
        public IActionResult GetTeams([FromRoute] String CourseId)
        {
            Course Course = Context.Courses.Find(CourseId);

            if (Course.Open == false)
            {
                TeamListSlotsViewModel VM = new TeamListSlotsViewModel();

                Int32 NrOfStudents = Context.CourseUsers.Where(x => x.IsApproved == true).Count(x => x.CourseId == CourseId);

                VM.CourseId = CourseId;
                VM.NrOfTeams = (Int32)Math.Ceiling((Double)NrOfStudents / Course.TeamSize);

                List<Team> Teams = Context.Teams.Where(t => t.CourseId == CourseId).ToList();
                VM.Teams = (Teams == null) ? new List<Team>() : Teams;

                return Ok(VM);
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
        }

        [HttpPost]
        [Route("teams")]
        public IActionResult PostTeam([FromBody] AddTeamViewModel VM)
        {
            Course Course = Context.Courses.Find(VM.CourseId);

            Team Team = new Team()
            {
                Name = VM.Name,
                Details = VM.Details,
                Course = Course
            };

            Console.WriteLine("Skill nr: " + VM.Questions.Count());
            foreach (Question Question in VM.Questions)
            {
                Console.WriteLine("Skill id: " + Question.SelectedValue);
                Skill Skill = Context.Skills.Find(Question.Id);

                if (Skill.Type == SkillType.Checkbox)
                {
                    Question.SelectedValue = new String("");
                    for (int i = 0; i < Question.CheckboxMultipleValues.Count(); ++i)
                    {
                        Boolean ValueSelected = Question.CheckboxMultipleValues[i];
                        Question.SelectedValue += ValueSelected ? Question.Values[i] + ";" : "";
                    }
                }

                TeamSkill TeamSkill = new TeamSkill()
                {
                    Team = Team,
                    Skill = Skill,
                    Value = Question.SelectedValue
                };

                Context.Add(TeamSkill);
            }

            Context.Add(Team);
            Context.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("teams/details/{TeamId}")]
        public IActionResult GetDetails([FromRoute] String TeamId)
        {
            Team Team = Context.Teams.Find(TeamId);

            if(Team != null)
            {
                return Ok(Team.Details);
            }

            Console.WriteLine("Team not found!");
            return StatusCode(StatusCodes.Status404NotFound);
        }

        [HttpGet]
        [Route("teams/skills/{CourseId}")]
        public IActionResult GetCourseSkills([FromRoute] String CourseId)
        {
            List<Skill> CourseSkills = Context.SkillCourses.Include(x => x.Skill)
                .Where(x => x.CourseId.Equals(CourseId)).Select(x => x.Skill).Include(x => x.SkillValues).ToList();

            Course Course = Context.Courses.Find(CourseId);

            if (CourseSkills.Count > 0)
            {
                return Ok(CourseSkills);
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
        }
    }
}