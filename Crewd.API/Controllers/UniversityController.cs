﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Crewd.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UniversityController : ControllerBase
    {
        private readonly UserManager<User> UserManager;
        private readonly CrewdDbContext Context;

        public UniversityController(UserManager<User> userManager, CrewdDbContext context)
        {
            UserManager = userManager;
            Context = context;
        }


        [HttpPost]
        public async Task<IActionResult> AddTeacher([FromBody] AddTeacherViewModel Data)
        {
            //Console.WriteLine("university endpoint reached ");
            var User = 
                Context
                    .Users
                    .Include(o => o.UserUniversities)
                    .ThenInclude(o => o.University)
                    .Where(u => u.UserName.GetHashCode().ToString().Equals(Data.Auth) && u.IsUniversity)
                    .FirstOrDefault();
            
            if (User != null)
            {
                Console.WriteLine("User registering teacher: " + User.FullName);
                User Teacher = new User
                {
                    FullName = Data.FullName,
                    UserName = Data.UserName,
                    IsUniversity = false
                };

                var university = 
                    User.UserUniversities
                        .FirstOrDefault();

                if (university == null)
                    return StatusCode(StatusCodes.Status406NotAcceptable);

                Console.WriteLine("Registering teacher for: " + university.University.Name);
                UserUniversity UserUniversity = new UserUniversity
                {
                    University = university.University,
                    User = Teacher,
                    IsTeacher = true
                };

            
                IdentityResult Result = await UserManager.CreateAsync(Teacher, Data.Password);

                Context.Add(UserUniversity);
                Context.SaveChanges();

                if (Result.Succeeded)
                {
                    return Ok();
                }
                Console.WriteLine(Result.Errors.ToList().ToString());        
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }

    }
}