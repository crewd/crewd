﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Crewd.API.Controllers
{
 
    public class AccountController : ControllerBase
    {
        private readonly UserManager<User> UserManager;
        private readonly SignInManager<User> SignInManager;
        private readonly CrewdDbContext Context;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, CrewdDbContext context)
        {
            Context = context;
            UserManager = userManager;
            SignInManager = signInManager;
        }

        [HttpPost]
        [Route("api/login")]
        public async Task<IActionResult> Login([FromBody] String [] authData)
        {
            Console.WriteLine("endpoint reached, "+authData[0]);

            
            var result = await SignInManager.PasswordSignInAsync(authData[0], authData[1], true, false);
            if (!result.Succeeded)
            {
                return StatusCode(404);
            }


            var claimsIdentity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, authData[0]),
                }, "Cookies");


            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            await Request.HttpContext.SignInAsync("Cookies", claimsPrincipal);
            Console.WriteLine("successful login", result.Succeeded);

            return Ok(authData[0].GetHashCode());
        }

        [HttpGet]
        [Route("api/usertype/{Auth}")]
        public IActionResult UserType([FromRoute] String Auth)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Auth)).FirstOrDefault();

            if(User != null)
            {
                if(User.IsUniversity)
                    return Ok("university");
                    
                UserUniversity UserUniversity = Context.UserUniversities.Where(uu => uu.UserId == User.Id).First();

                if(UserUniversity != null && UserUniversity.IsTeacher == true)
                {
                    return Ok("teacher");
                }

                return Ok("user");
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
           
        }

        [HttpPost]
        [Route("api/logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return NoContent();
        }

        [HttpPost]
        [Route("api/register")]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel data)
        {
            if (data.Password == data.ConfirmedPassword)
            {
                Console.WriteLine("password not matching in");
                BadRequest();
            }

            var uniId = Context.Universities.Where(u => u.Name == data.University).FirstOrDefault();
            if (uniId == null)
            {
                Console.WriteLine("university not existing");
                BadRequest();
            }

            var user = new User
                {
                    UserName = data.UserName,
                    FullName = data.FullName,
                    Email = data.Email,
                    IsUniversity = false,
                    UserUniversities = new List<UserUniversity>{ new UserUniversity { UniversityId = uniId.Id, IsTeacher = false } }
                };
            var pw = data.Password;

            var result = UserManager.CreateAsync(user, pw).Result;
            

            if (!result.Succeeded)
            {
                return StatusCode(404);
            }
            Console.WriteLine(@"${data.FullName} is registered");
            return Ok();
        }


    }
}