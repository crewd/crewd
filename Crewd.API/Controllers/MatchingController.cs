﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Crewd.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Crewd.API.Controllers
{
    [Route("api/")]
    [ApiController]
    public class MatchingController : ControllerBase
    {
        private readonly CrewdDbContext Context;

        public MatchingController(CrewdDbContext _context)
        {
            Context = _context;
        }


        //Get datas from the database to start the Gale-Shapley algorithm
        [HttpGet]
        [Route("matching/{CourseID}")]
        public Dictionary<string, List<string>> runGaleShapley([FromRoute] string CourseID)
        //public IActionResult runGaleShapley([FromRoute] string CourseID)
        {
            
            var a = Context.getCourses();
            //FakeDbSet<Course> course = (FakeDbSet<Course>)Context.Courses;
            //var c = course.Local;
            //int teamSize = c.First(o => o.Id == CourseID).TeamSize ;
            int teamSize = Context.getCourses().First(o => o.Id == CourseID).TeamSize;

            Dictionary<string, Dictionary<string, List<string>>> studentsAnswers = new Dictionary<string, Dictionary<string, List<string>>>();
            Dictionary<string, Dictionary<string, List<string>>> teamsAnswers = new Dictionary<string, Dictionary<string, List<string>>>();
            List<int> weights = new List<int>();
            List<SkillType> types = new List<SkillType>();

            // TODO   :    Fill dictionaries/lists...
            //FakeDbSet<CourseUser> cUser = (FakeDbSet<CourseUser>)Context.CourseUsers;
            //var cU = cUser.Local;
            //var users = cU.Where(o=> o.CourseId == CourseID).Select(o => o.UserId).ToList();
            var skillcourse = Context.getSkillCourses().Where(p => p.CourseId == CourseID).Select(o => o.SkillId).ToList();

            
            var users = Context.getCourseUsers().Where(o => o.CourseId == CourseID && o.IsTeacher == false).Select(o => o.User).ToList();
            Console.WriteLine("Nr of Course users: " + users.Count() + "/");
            foreach (var user in users)
            {
                //FakeDbSet<UserSkill> uSkill = (FakeDbSet<UserSkill>)Context.UserSkills;
                //var uS = uSkill.Local;
                //var answers = uS.Where(o=> o.UserId == user).Select(o => o.Value).ToList();
                Console.WriteLine("Course user: " + user.Id + "/ ");
                var answers = Context.getUserSkills().Where(o => o.User == user).Select(o => o.Value).Intersect(skillcourse).ToList();
                
                Dictionary<string, List<string>> quest = new Dictionary<string, List<string>>();
                int i = 0;
                foreach (string ans in answers)
                {
                    i++;
                    Console.WriteLine(ans);
                    Console.WriteLine(ans.Split('#'));
                    quest.Add("q"+i, ans.Split('#').ToList());
                }
                studentsAnswers.Add(user.FullName, quest);
            }


            //FakeDbSet<Team> tm = (FakeDbSet<Team>)Context.Teams;
            //var t = tm.Local;
            //var teams = t.Where(o => o.CourseId == CourseID).Select(o => o.Id).ToList();

            var teams = Context.getTeams().Where(o => o.CourseId == CourseID).Select(o => o.Name).ToList();
            foreach (var team in teams)
            {
                //FakeDbSet<TeamSkill> tSkill = (FakeDbSet<TeamSkill>)Context.TeamSkills;
                //var tS = tSkill.Local;
                //var answers = tS.Where(o => o.TeamId == team).Select(o => o.Value).ToList();
                var answers = Context.getTeamSkills().Where(o => o.Team.Name == team).Select(o => o.Value).ToList();
                Dictionary<string, List<string>> quest = new Dictionary<string, List<string>>();
                int i = 0;
                foreach (string ans in answers)
                {
                    i++;
                    quest.Add("q" + i, ans.Split('#').ToList());
                }
                teamsAnswers.Add(team, quest);
            }

            //FakeDbSet<SkillCourse> sCourse = (FakeDbSet<SkillCourse>)Context.SkillCourses;
            //var sC = sCourse.Local;
            //List<string> skillIDs = sC.Where(o => o.CourseId == CourseID).Select(o => o.SkillId).ToList();

            List<string> skillIDs = Context.getSkillCourses().Where(o => o.CourseId == CourseID).Select(o => o.SkillId).ToList();

            //FakeDbSet<Skill> skill = (FakeDbSet<Skill>)Context.Skills;
            //var sk = skill.Local;
            foreach (string s in skillIDs)
            {
                weights.AddRange(Context.getSkills().Where(o => o.Id == s).Select(o => o.Weight).ToList());
                types.AddRange(Context.getSkills().Where(o => o.Id == s).Select(o => o.Type).ToList());
            }

            List<Dictionary<string, List<string>>> prefLists = createPrefLists(studentsAnswers, teamsAnswers, weights, types);
            Dictionary<string, List<string>> Result = removeFakes(GS(teamSize, prefLists[1], prefLists[0]));

            Console.WriteLine("Returning result, count:" + Result.Count());
            //return Ok(Result);
            return Result;
        }


       //Remove fakes from the result
       public Dictionary<string, List<string>> removeFakes(Dictionary<string, List<string>> result)
        {
            Dictionary<string, List<string>> newResult = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, List<string>> p in result)
            {
                List<string> members = new List<string>();
                foreach (string s in p.Value)
                {
                    if (!s.Contains("fake"))
                    {
                        members.Add(s);
                    }
                }
                newResult.Add(p.Key, members);
            }

            return newResult;
        }






        //Gale-Shapley algorithm

        public Dictionary<string, List<string>> GS(int teamSize, Dictionary<string, List<string>> studentsPrefList, Dictionary<string, List<string>> teamsPrefList)
        {
            //Complete the students to have correct size for the teams
            createFakeStudents(teamSize, studentsPrefList, teamsPrefList);

            //Create the result dictionary, that will contain the teams with the team members after the stable matching
            Dictionary<string, List<string>> result = initializeResult(teamsPrefList);
            //Create a list, it will contain those, who hasn't team yet. First it contains everybody.
            List<string> needPlace = initializeNeedPlace(studentsPrefList);

            //Iterate while there is an uncompleted team, or anybody, who have no team
            while (hasFreePlace(teamSize, result) || needPlace.Count() > 0)
            {
                //every student join his/her favorite team, which don't refused him/her yet
                foreach (String student in needPlace)
                {
                    joinTeam(student, studentsPrefList[student], result);

                }
                //now nobody need team
                needPlace.Clear();

                //In every team's case, we check, that it has the correct size of team members
                for (int i = 0; i < result.Keys.Count; i++)
                {
                    if (result.ElementAt(i).Value.Count > teamSize)
                    {
                        //if a team have more member, than the maximum, we need to refuse the team's least favorites of them
                        List<string> refused = refuse(result.ElementAt(i).Key, result.ElementAt(i).Value, teamsPrefList[result.ElementAt(i).Key], studentsPrefList, teamSize);
                        //the refused ones, need to search for another team 
                        needPlace.AddRange(refused);
                        result[result.ElementAt(i).Key] = result[result.ElementAt(i).Key].Except(refused).ToList();

                    }


                }
            }


            return result;
        }

        //refuse so much team members as are more than the max teamsize, and it pulls out from their preflist this team
        public List<String> refuse(String team, List<string> members, List<string> teamPrefList, Dictionary<string, List<string>> studentsPerfList, int teamSize)
        {
            List<String> refused = new List<string>();
            int i = members.Count - teamSize;
            for (int j = teamPrefList.Count - 1; j >= 0 && i > 0; j--)
            {
                if (members.Contains(teamPrefList[j]))
                {
                    refused.Add(teamPrefList[j]);
                    studentsPerfList[teamPrefList[j]].Remove(team);
                    i--;
                }
            }


            return refused;
        }

        //create a dictionary with every team, and empty members list
        public Dictionary<string, List<string>> initializeResult(Dictionary<string, List<string>> teamsPrefList)
        {
            Dictionary<string, List<string>> algo = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, List<string>> entry in teamsPrefList)
            {
                List<string> prefList = new List<string>();
                algo.Add(entry.Key, prefList);
            }
            return algo;
        }

        //create a list with all of the students
        public List<string> initializeNeedPlace(Dictionary<string, List<string>> studentsPerfList)
        {
            List<string> needPlace = new List<string>();
            foreach (KeyValuePair<string, List<string>> entry in studentsPerfList)
            {
                needPlace.Add(entry.Key);
            }
            return needPlace;
        }

        //add a student to a team
        public void joinTeam(String student, List<String> prefList, Dictionary<string, List<string>> result)
        {
            result[prefList[0]].Add(student);
        }

        //create as much fake student as need, so complete the students to have "teamcount*teamsize" student, these students will be in the end of all the teams pref list
        public void createFakeStudents(int teamSize, Dictionary<String, List<String>> studentsPerfList, Dictionary<String, List<String>> teamsPrefList)
        {
            //Add fake students 
            int fakeStudentCount = teamsPrefList.Count() * teamSize - studentsPerfList.Count();
            for (int i = 0; i < fakeStudentCount; i++)
            {
                List<string> prefList = new List<string>();
                int index = 0;
                foreach (KeyValuePair<String, List<String>> entry in teamsPrefList)
                {
                    prefList.Add(entry.Key);
                    index = i + 1;
                    entry.Value.Add("fake" + index);
                }
                studentsPerfList.Add("fake" + index, prefList);
            }

        }

        //returns that is any team, who need teammember
        public bool hasFreePlace(int teamSize, Dictionary<String, List<String>> result)
        {
            bool hasPlace = false;
            foreach (KeyValuePair<string, List<string>> entry in result)
            {
                if (entry.Value.Count < teamSize)
                {
                    hasPlace = true;
                }
            }
            return hasPlace;
        }





        //pref list:

        //create pref lists:
        public List<Dictionary<string, List<string>>> createPrefLists(Dictionary<string, Dictionary<string, List<string>>> studentsAnswers, Dictionary<string, Dictionary<string, List<string>>> teamsAnswers, List<int> weights, List<SkillType> types)
        {

            List<Dictionary<string, List<string>>> result = new List<Dictionary<string, List<string>>>();

            Dictionary<string, List<string>> teamsPerfList = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> studentsPerfList = new Dictionary<string, List<string>>();

            int[,] points = new int[studentsAnswers.Count, teamsAnswers.Count];

            for (int i = 0; i < studentsAnswers.Count; i++)
            {
                for (int j = 0; j < teamsAnswers.Count; j++)
                {
                    points[i, j] = calculate(studentsAnswers.ElementAt(i), teamsAnswers.ElementAt(j), weights, types);
                }

                studentsPerfList.Add(studentsAnswers.Keys.ElementAt(i), makeSortedList(Enumerable.Range(0, points.GetLength(1)).Select(x => points[i, x]).ToList(), teamsAnswers.Keys.ToList()));
            }
            for (int j = 0; j < teamsAnswers.Count; j++)
            {
                teamsPerfList.Add(teamsAnswers.Keys.ElementAt(j), makeSortedList(Enumerable.Range(0, points.GetLength(0)).Select(x => points[x, j]).ToList(), studentsAnswers.Keys.ToList()));
            }

            result.Add(teamsPerfList);
            result.Add(studentsPerfList);
            return result;
        }

        //Order the possibilities by the points
        public List<string> makeSortedList(List<int> points, List<string> possibilities)
        {
            var result = possibilities.Zip(points, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v);
            var l = result.OrderByDescending(value => value.Value).ToDictionary(x => x.Key, x => x.Value); ;
            //var dic = l.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
            //possibilities = possibilities.OrderBy(d => points.IndexOf(d.Id)).ToList();
            return l.Keys.ToList();
        }

        //Calculate the points of the answers by types
        
        public int calculate([FromQuery] KeyValuePair<string, Dictionary<string, List<string>>> s, [FromQuery] KeyValuePair<string, Dictionary<string, List<string>>> t, [FromQuery] List<int> weights, [FromQuery] List<SkillType> types)
        {
            int point = 0;
            for (int i = 0; i < s.Value.Count; i++)
            {
                switch (types[i])
                {
                    case SkillType.Checkbox: point += calculateCheckbox(s.Value.ElementAt(i).Value, t.Value.ElementAt(i).Value, weights[i]); break;
                    case SkillType.Dropdown: point += calculateDropdown(s.Value.ElementAt(i).Value, t.Value.ElementAt(i).Value, weights[i]); break;
                    default: point += calculateRadioButton(s.Value.ElementAt(i).Value, t.Value.ElementAt(i).Value, weights[i]); break;
                }
            }

            return point;
        }

        public int calculateCheckbox(List<string> sAns, List<string> tAns, int weight)
        {
            int result = 0;
            foreach (string s in tAns)
            {
                if (sAns.Contains(s))
                {
                    result += weight / tAns.Count();
                }
            }
            return result;
        }

        public int calculateDropdown(List<string> sAns, List<string> tAns, int weight)
        {
            if (sAns[0].Equals(tAns[0]))
            {
                return weight;
            }
            else
            {
                return 0;
            }
            
        }

        public int calculateRadioButton(List<string> sAns, List<string> tAns, int weight)
        {
            if (sAns[0].Equals(tAns[0]))
            {
                return weight;
            }
            else
            {
                return 0;
            }
            
        }

        
    }

}