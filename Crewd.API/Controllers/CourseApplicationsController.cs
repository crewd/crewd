﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Crewd.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Crewd.API.Controllers
{
    [Route("api/applications/")]
    [ApiController]
    public class CourseApplicationsController : ControllerBase
    {
        private readonly CrewdDbContext Context;

        public CourseApplicationsController(CrewdDbContext context)
        {
            Context = context;
        }

        // Get the Skills belonging to a specific Course for the Join page
        // Authorized only if: user is not a teacher, course is open and this user hadn't already joined 
        [HttpGet]
        [Route("skills/{CourseId}/{Auth}")]
        public IActionResult GetCourseSkills([FromRoute] String CourseId, [FromRoute] String Auth)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Auth)).FirstOrDefault();
            Course Course = Context.Courses.Find(CourseId);

            if(User == null)
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
            else if (Context.UserUniversities.Any(x => x.UserId == User.Id && x.IsTeacher == true))
            {
                var Response = new NotFoundObjectResult("Teachers cannot join courses!");
                return Response;
            }
            else if(Course.Open == false)
            {
                var Response = new NotFoundObjectResult("This course has been closed!");
                return Response;
            }
            else if(Context.CourseUsers.Any(x => x.UserId == User.Id && x.CourseId == Course.Id))
            {
                var Response = new NotFoundObjectResult("You have already applied to this course!");
                return Response;
            }
            else
            {
                List<Skill> CourseSkills = Context.SkillCourses.Include(x => x.Skill)
                    .Where(x => x.CourseId.Equals(CourseId)).Select(x => x.Skill).Include(x => x.SkillValues).ToList();               

                if (CourseSkills.Count > 0)
                {
                    return Ok(CourseSkills);
                }             
            }

            return StatusCode(StatusCodes.Status404NotFound);
        }

        // Get applications to the courses created by the currently logged in teacher
        [HttpGet]
        [Route("{Auth}")]
        public IActionResult GetCourseApplications([FromRoute] String Auth)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(Auth)).FirstOrDefault();
            if (User != null)
            {
                List<Course> Courses = Context.CourseUsers.Include(cu => cu.Course).Where(cu => cu.UserId == User.Id && cu.IsTeacher == true).
                    Select(cu => cu.Course).ToList();

                List<CourseUser> Applications = new List<CourseUser>();
               
                Console.WriteLine("Number of courses for this teacher: " + Courses.Count());

                if (Courses != null && Courses.Count() > 0)
                {                  
                    Applications = Context.CourseUsers.Include(x => x.User).Include(x => x.Course)
                        .Where(x => x.IsApproved == false && x.IsTeacher == false && Courses.Contains(x.Course)).ToList();               
                }

                Console.WriteLine("Number of applications for the courses: " + Applications.Count());

                return Ok(Applications);
            }
           
            return StatusCode(StatusCodes.Status403Forbidden);         
        }

        // Process the data coming from a course application
        // UserSkill and CourseUser entries are to be created
        [HttpPost]
        public IActionResult AddApplication([FromBody] JoinViewModel VM)
        {
            var User = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(VM.Auth)).FirstOrDefault();

            // Teachers cannot join courses
            if (User != null && !Context.UserUniversities.Any(x => x.UserId == User.Id && x.IsTeacher == true))
            {
                Console.WriteLine("Submitting application. Course id: " + VM.CourseId);
                var user = Context.Users.Where(u => u.UserName.GetHashCode().ToString().Equals(VM.Auth)).FirstOrDefault();

                // Add entry to CourseUser table
                CourseUser CourseUser = new CourseUser()
                {
                    Course = Context.Courses.Find(VM.CourseId),
                    User = user,
                    IsApproved = false,
                    IsTeacher = false
                };
                Context.Add(CourseUser);

                // Add entries to UserSkills table
                foreach (Question Question in VM.Questions)
                {
                    Skill Skill = Context.Skills.Find(Question.Id);

                    if (Skill.Type == SkillType.Checkbox)
                    {
                        Question.SelectedValue = new String("");
                        for (int i = 0; i < Question.CheckboxMultipleValues.Count(); ++i)
                        {
                            Boolean ValueSelected = Question.CheckboxMultipleValues[i];
                            Question.SelectedValue += ValueSelected ? Question.Values[i] + ";" : "";
                        }
                    }


                    UserSkill UserSkill = new UserSkill()
                    {
                        Skill = Skill,
                        User = user,
                        Value = Question.SelectedValue
                    };
                    Context.Add(UserSkill);
                }

                Context.SaveChanges();
            }

            return Ok();
        }

        [HttpPut]
        [Route("approve/{Id}")]
        public IActionResult Approve([FromRoute]String Id)
        {
            CourseUser CourseUser = Context.CourseUsers.Find(Id);
            if (CourseUser != null)
            {
                CourseUser.IsApproved = true;
                Context.SaveChanges();
                return Ok();
            }

            return StatusCode(StatusCodes.Status404NotFound);
        }

        [HttpPut]
        [Route("reject/{Id}")]
        public IActionResult Reject([FromRoute]String Id)
        {
            Console.WriteLine(Id);

            CourseUser CourseUser = Context.CourseUsers.Find(Id);
            if (CourseUser != null)
            {
                Context.Remove(CourseUser);
                Context.SaveChanges();
                return Ok();
            }

            return Ok();
        }
    }
}