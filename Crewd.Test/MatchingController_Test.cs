﻿using System.Collections.Generic;
using System.Text;
using System;
using Xunit;
using Crewd.API.Controllers;
using Moq;
using Crewd.Persistence;
using Microsoft.EntityFrameworkCore;
using FluentAssertions;
using System.Linq;

namespace Crewd.Test
{
 
    public class MatchingController_Test
    {
        //Setup
        //private static DbContextOptions<CrewdDbContext> options = new DbContextOptionsBuilder<CrewdDbContext>()
        //  .UseInMemoryDatabase(databaseName: "TestDb1")
         // .Options;

        private static Mock<CrewdDbContext> mockDB = new Mock<CrewdDbContext>();
        MatchingController controller = new MatchingController(mockDB.Object);



        //public ActionResult<Dictionary<string, List<string>>> runGaleShapley(string CourseName)
        [Fact]
        public void runGaleShapley_Test()
        {
            List<Course> c = new List<Course>();
            c.Add(new Course { Id = "1", TeamSize = 2 });
            c.Add(new Course { Id = "2", TeamSize = 4 });
            c.Add(new Course { Id = "3", TeamSize = 1 });
            mockDB.Setup(m => m.getCourses()).Returns(c);


            List<CourseUser> courseUsers = new List<CourseUser>();
            courseUsers.Add(new CourseUser { CourseId = "1", UserId = "s1", User = new User { FullName = "Bartha Patricia" } });
            courseUsers.Add(new CourseUser { CourseId = "1", UserId = "s2", User = new User { FullName = "Pál Tamás" } });
            courseUsers.Add(new CourseUser { CourseId = "2", UserId = "s3", User = new User { FullName = "Egor Korshunov" } });
            courseUsers.Add(new CourseUser { CourseId = "1", UserId = "s4", User = new User { FullName = "Szalóki SÁndor" } });
            mockDB.Setup(m => m.getCourseUsers()).Returns(courseUsers);

            List<UserSkill> userSkills = new List<UserSkill>();
            userSkills.Add(new UserSkill { UserId = "s1", Value = "a", User = new User { FullName = "Bartha Patricia" } });
            userSkills.Add(new UserSkill { UserId = "s1", Value = "b", User = new User { FullName = "Bartha Patricia" } });
            userSkills.Add(new UserSkill { UserId = "s1", Value = "a#c#d", User = new User { FullName = "Bartha Patricia" } });
            userSkills.Add(new UserSkill { UserId = "s1", Value = "a", User = new User { FullName = "Bartha Patricia" } });
            userSkills.Add(new UserSkill { UserId = "s2", Value = "b", User = new User { FullName = "Pál Tamás" } });
            userSkills.Add(new UserSkill { UserId = "s2", Value = "e", User = new User { FullName = "Pál Tamás" } });
            userSkills.Add(new UserSkill { UserId = "s2", Value = "f#d#h", User = new User { FullName = "Pál Tamás" } });
            userSkills.Add(new UserSkill { UserId = "s2", Value = "a", User = new User { FullName = "Pál Tamás" } });
            userSkills.Add(new UserSkill { UserId = "s3", Value = "e", User = new User { FullName = "Egor Korshunov" } });
            userSkills.Add(new UserSkill { UserId = "s3", Value = "f#d#h", User = new User { FullName = "Egor Korshunov" } });
            userSkills.Add(new UserSkill { UserId = "s3", Value = "a", User = new User { FullName = "Egor Korshunov" } });
            userSkills.Add(new UserSkill { UserId = "s4", Value = "a", User = new User { FullName = "Szalóki SÁndor" } });
            userSkills.Add(new UserSkill { UserId = "s4", Value = "c", User = new User { FullName = "Szalóki SÁndor" } });
            userSkills.Add(new UserSkill { UserId = "s4", Value = "b#c#d", User = new User { FullName = "Szalóki SÁndor" } });
            userSkills.Add(new UserSkill { UserId = "s4", Value = "b", User = new User { FullName = "Szalóki SÁndor" } });
            mockDB.Setup(m => m.getUserSkills()).Returns(userSkills);

            List<Team> teams = new List<Team>();
            teams.Add(new Team { CourseId = "2" , Id= "t1", Name = "Team A" } );
            teams.Add(new Team { CourseId = "1", Id = "t2", Name = "Team B" });
            teams.Add(new Team { CourseId = "1", Id = "t3", Name = "Team C" });
            mockDB.Setup(m => m.getTeams()).Returns(teams);

            List<TeamSkill> teamSkills = new List<TeamSkill>();
            teamSkills.Add(new TeamSkill { TeamId = "t1", Value = "a", Team = new Team { Name = "Team A"} });
            teamSkills.Add(new TeamSkill { TeamId = "t1", Value = "e", Team = new Team { Name = "Team A" } });
            teamSkills.Add(new TeamSkill { TeamId = "t1", Value = "a#f#d", Team = new Team { Name = "Team A" } });
            teamSkills.Add(new TeamSkill { TeamId = "t1", Value = "a", Team = new Team { Name = "Team A" } });
            teamSkills.Add(new TeamSkill { TeamId = "t2", Value = "a", Team = new Team { Name = "Team B" } });
            teamSkills.Add(new TeamSkill { TeamId = "t2", Value = "b", Team = new Team { Name = "Team B" } });
            teamSkills.Add(new TeamSkill { TeamId = "t2", Value = "a#c#d", Team = new Team { Name = "Team B" } });
            teamSkills.Add(new TeamSkill { TeamId = "t2", Value = "a", Team = new Team { Name = "Team B" } });
            teamSkills.Add(new TeamSkill { TeamId = "t3", Value = "c", Team = new Team { Name = "Team C" } });
            teamSkills.Add(new TeamSkill { TeamId = "t3", Value = "a", Team = new Team { Name = "Team C" } });
            teamSkills.Add(new TeamSkill { TeamId = "t3", Value = "a#b#h", Team = new Team { Name = "Team C" } });
            teamSkills.Add(new TeamSkill { TeamId = "t3", Value = "e", Team = new Team { Name = "Team C" } });
            mockDB.Setup(m => m.getTeamSkills()).Returns(teamSkills);

            List<SkillCourse> skillCourse = new List<SkillCourse>();
            skillCourse.Add(new SkillCourse { CourseId = "1", SkillId = "1"});
            skillCourse.Add(new SkillCourse { CourseId = "1", SkillId = "2" });
            skillCourse.Add(new SkillCourse { CourseId = "1", SkillId = "3" });
            skillCourse.Add(new SkillCourse { CourseId = "1", SkillId = "4" });
            skillCourse.Add(new SkillCourse { CourseId = "2", SkillId = "5" });
            skillCourse.Add(new SkillCourse { CourseId = "2", SkillId = "6" });
            mockDB.Setup(m => m.getSkillCourses()).Returns(skillCourse);

            List<Skill> skills = new List<Skill>();
            skills.Add(new Skill { Id = "1", Type = SkillType.Dropdown, Weight = 10});
            skills.Add(new Skill { Id = "2", Type = SkillType.RadioButton, Weight = 15 });
            skills.Add(new Skill { Id = "3", Type = SkillType.Checkbox, Weight = 50 });
            skills.Add(new Skill { Id = "4", Type = SkillType.Dropdown, Weight = 25 });
            skills.Add(new Skill { Id = "5", Type = SkillType.Checkbox, Weight = 30 });
            skills.Add(new Skill { Id = "6", Type = SkillType.Dropdown, Weight = 28 });
            mockDB.Setup(m => m.getSkills()).Returns(skills);

            var result =  controller.runGaleShapley("1");


            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> t1 = new List<string>();
            t1.Add("Bartha Patricia");
            t1.Add("Pál Tamás");
            expected.Add("Team B", t1);
            List<string> t2 = new List<string>();
            t2.Add("Szalóki SÁndor");
            expected.Add("Team C", t2);


            expected.Should().BeEquivalentTo(result);
        }




        //public Dictionary<string, List<string>> removeFakes(Dictionary<string, List<string>> result)
        //no need to remove
        [Fact]
        public void removeFakes_noFake_Test()
        {
            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> t1 = new List<string>();
            t1.Add("Bartha Patricia");
            t1.Add("Pál Tamás");
            expected.Add("Team B", t1);
            List<string> t2 = new List<string>();
            t2.Add("Szalóki SÁndor");
            expected.Add("Team C", t2);

            Dictionary<string, List<string>> original = new Dictionary<string, List<string>>();
            List<string> o1 = new List<string>();
            o1.Add("Bartha Patricia");
            o1.Add("Pál Tamás");
            original.Add("Team B", o1);
            List<string> o2 = new List<string>();
            o2.Add("Szalóki SÁndor");
            original.Add("Team C", o2);

            var result = controller.removeFakes(original);

            expected.Should().BeEquivalentTo(result);
        }

        //need to remove some fakes
        [Fact]
        public void removeFakes_someFake_Test()
        {
            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> t1 = new List<string>();
            t1.Add("Bartha Patricia");
            t1.Add("Pál Tamás");
            expected.Add("Team B", t1);
            List<string> t2 = new List<string>();
            t2.Add("Szalóki SÁndor");
            expected.Add("Team C", t2);

            Dictionary<string, List<string>> original = new Dictionary<string, List<string>>();
            List<string> o1 = new List<string>();
            o1.Add("Bartha Patricia");
            o1.Add("Pál Tamás");
            o1.Add("fake3");
            original.Add("Team B", o1);
            List<string> o2 = new List<string>();
            o2.Add("Szalóki SÁndor");
            o2.Add("fake1");
            o2.Add("fake2");
            original.Add("Team C", o2);

            var result = controller.removeFakes(original);

            expected.Should().BeEquivalentTo(result);
        }




        //public Dictionary<string, List<string>> GS(int teamSize, Dictionary<string, List<string>> studentsPerfList, Dictionary<string, List<string>> teamsPrefList)
        //teamsize*teams = students count
        [Fact]
        public void GS_placesEqualsToStudents_Test()
        {
            //CREATE TEST VALUES FOR THE GS PARAMETERS
            Dictionary<string, List<string>> studentsPerfList = new Dictionary<string, List<string>>();
            List<string> spref = new List<string>();
            List<string> spref2 = new List<string>();
            List<string> spref3 = new List<string>();
            spref.Add("t1");
            spref2.Add("t1");
            spref.Add("t2");
            spref2.Add("t2");
            studentsPerfList.Add("s1", spref);
            studentsPerfList.Add("s2", spref2);
            Dictionary<string, List<string>> teamsPerfList = new Dictionary<string, List<string>>();
            List<string> tpref = new List<string>();
            List<string> tpref2 = new List<string>();
            tpref.Add("s1");
            tpref.Add("s2");
            teamsPerfList.Add("t1", tpref);
            tpref2.Add("s2");
            tpref2.Add("s1");
            teamsPerfList.Add("t2", tpref2);

            
            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> t1L = new List<string>();
            t1L.Add("s1");
            expected.Add("t1", t1L);
            List<string> t2L = new List<string>();
            t2L.Add("s2");
            expected.Add("t2", t2L);

            expected.Should().BeEquivalentTo(controller.GS(1, studentsPerfList, teamsPerfList));
        }

        //teamsize*teams > students count -> we need fake students
        [Fact]
        public void GS_morePlaceThanStudent_Test()
        {
            //CREATE TEST VALUES FOR THE GS PARAMETERS
            Dictionary<string, List<string>> studentsPerfList = new Dictionary<string, List<string>>();
            List<string> spref = new List<string>();
            List<string> spref2 = new List<string>();
            List<string> spref3 = new List<string>();
            spref.Add("t1");
            spref2.Add("t1");
            spref.Add("t2");
            spref2.Add("t2");
            studentsPerfList.Add("s1", spref);
            studentsPerfList.Add("s2", spref2);
            spref3.Add("t2");
            spref3.Add("t1");
            studentsPerfList.Add("s3",spref3);
            Dictionary<string, List<string>> teamsPerfList = new Dictionary<string, List<string>>();
            List<string> tpref = new List<string>();
            List<string> tpref2 = new List<string>();
            tpref.Add("s1");
            tpref.Add("s2");
            tpref.Add("s3");
            teamsPerfList.Add("t1", tpref);
            tpref2.Add("s3");
            tpref2.Add("s2");
            tpref2.Add("s1");
            teamsPerfList.Add("t2", tpref2);


            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> t1L = new List<string>();
            t1L.Add("s1");
            t1L.Add("s2");
            expected.Add("t1", t1L);
            List<string> t2L = new List<string>();
            t2L.Add("s3");
            t2L.Add("fake1");
            expected.Add("t2", t2L);

            //var res = controller.GS(2, studentsPerfList, teamsPerfList);

            expected.Should().BeEquivalentTo(controller.GS(2, studentsPerfList, teamsPerfList));
        }

        //complex example for 1-1
        [Fact]
        public void GS_complexExample_Test()
        {
            //CREATE TEST VALUES FOR THE GS PARAMETERS
            Dictionary<string, List<string>> studentsPerfList = new Dictionary<string, List<string>>();
            List<string> spref = new List<string>();
            List<string> spref2 = new List<string>();
            List<string> spref3 = new List<string>();
            List<string> spref4 = new List<string>();
            List<string> spref5 = new List<string>();

            spref.Add("t3");
            spref.Add("t2");
            spref.Add("t5");
            spref.Add("t1");
            spref.Add("t4");

            spref2.Add("t1");
            spref2.Add("t2");
            spref2.Add("t5");
            spref2.Add("t3");
            spref2.Add("t4");

            spref3.Add("t4");
            spref3.Add("t3");
            spref3.Add("t2");
            spref3.Add("t1");
            spref3.Add("t5");

            spref4.Add("t1");
            spref4.Add("t3");
            spref4.Add("t4");
            spref4.Add("t2");
            spref4.Add("t5");

            spref5.Add("t1");
            spref5.Add("t2");
            spref5.Add("t4");
            spref5.Add("t5");
            spref5.Add("t3");

            studentsPerfList.Add("s1", spref);
            studentsPerfList.Add("s2", spref2);
            studentsPerfList.Add("s3", spref3);
            studentsPerfList.Add("s4", spref4);
            studentsPerfList.Add("s5", spref5);

            Dictionary<string, List<string>> teamsPerfList = new Dictionary<string, List<string>>();
            List<string> tpref = new List<string>();
            List<string> tpref2 = new List<string>();
            List<string> tpref3 = new List<string>();
            List<string> tpref4 = new List<string>();
            List<string> tpref5 = new List<string>();

            tpref.Add("s3");
            tpref.Add("s5");
            tpref.Add("s2");
            tpref.Add("s1");
            tpref.Add("s4");

            tpref2.Add("s5");
            tpref2.Add("s2");
            tpref2.Add("s1");
            tpref2.Add("s4");
            tpref2.Add("s3");

            tpref3.Add("s4");
            tpref3.Add("s3");
            tpref3.Add("s5");
            tpref3.Add("s1");
            tpref3.Add("s2");

            tpref4.Add("s1");
            tpref4.Add("s2");
            tpref4.Add("s3");
            tpref4.Add("s4");
            tpref4.Add("s5");

            tpref5.Add("s2");
            tpref5.Add("s3");
            tpref5.Add("s4");
            tpref5.Add("s2");
            tpref5.Add("s5");

            teamsPerfList.Add("t1", tpref);
            teamsPerfList.Add("t2", tpref2);
            teamsPerfList.Add("t3", tpref3);
            teamsPerfList.Add("t4", tpref4);
            teamsPerfList.Add("t5", tpref5);

            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> t1L = new List<string>();
            t1L.Add("s5");
            expected.Add("t1", t1L);
            List<string> t2L = new List<string>();
            t2L.Add("s2");
            expected.Add("t2", t2L);

            List<string> t3L = new List<string>();
            t3L.Add("s4");
            expected.Add("t3", t3L);
            List<string> t4L = new List<string>();
            t4L.Add("s3");
            expected.Add("t4", t4L);
            List<string> t5L = new List<string>();
            t5L.Add("s1");
            expected.Add("t5", t5L);

            //var res = controller.GS(1, studentsPerfList, teamsPerfList);

            expected.Should().BeEquivalentTo(controller.GS(1, studentsPerfList, teamsPerfList));
        }





        //public List<String> refuse(String team, List<string> members, List<string> teamPrefList, Dictionary<string, List<string>> studentsPerfList, int teamSize)
        //refuse one member
        [Fact]
        public void refuse_oneMember_Test()
        {
            String team = "t1";
            List<string> members = new List<string>();
            members.Add("s1");
            members.Add("s3");
            members.Add("s4");
            members.Add("fake1");

            List<string> teamPrefList = new List<string>();
            teamPrefList.Add("s1");
            teamPrefList.Add("s2");
            teamPrefList.Add("s3");
            teamPrefList.Add("s4");
            teamPrefList.Add("fake1");
            teamPrefList.Add("fake2");

            Dictionary<string, List<string>> studensPrefList = new Dictionary<string, List<string>>();
            List<string> s1L = new List<string>();
            s1L.Add("t1");
            s1L.Add("t2");
            studensPrefList.Add("s1", s1L);
            studensPrefList.Add("s2", s1L);
            studensPrefList.Add("s3", s1L);
            studensPrefList.Add("s4", s1L);
            studensPrefList.Add("fake1", s1L);
            studensPrefList.Add("fake2", s1L);

            int teamSize = 3;


            List<string> refused = new List<string>();
            refused.Add("fake1");

            List<string> prefList = new List<string>();
            prefList.Add("t2");

            refused.Should().BeEquivalentTo(controller.refuse(team, members, teamPrefList, studensPrefList, teamSize));
            studensPrefList["fake1"].Should().Equal(prefList);
        }

        //No need to refuse
        [Fact]
        public void refuse_noOne_Test()
        {
            String team = "t1";
            List<string> members = new List<string>();
            members.Add("s1");
            members.Add("s3");
            members.Add("s4");

            List<string> teamPrefList = new List<string>();
            teamPrefList.Add("s1");
            teamPrefList.Add("s2");
            teamPrefList.Add("s3");
            teamPrefList.Add("s4");
            teamPrefList.Add("fake1");
            teamPrefList.Add("fake2");

            Dictionary<string, List<string>> studensPrefList = new Dictionary<string, List<string>>();
            List<string> s1L = new List<string>();
            s1L.Add("t1");
            s1L.Add("t2");
            studensPrefList.Add("s1", s1L);
            studensPrefList.Add("s2", s1L);
            studensPrefList.Add("s3", s1L);
            studensPrefList.Add("s4", s1L);
            studensPrefList.Add("fake1", s1L);
            studensPrefList.Add("fake2", s1L);

            int teamSize = 3;


            List<string> refused = new List<string>();

            refused.Should().BeEquivalentTo(controller.refuse(team, members, teamPrefList, studensPrefList, teamSize));
        }

        //refuse more member
        [Fact]
        public void refuse_moreMember_Test()
        {
            String team = "t1";
            List<string> members = new List<string>();
            members.Add("s1");
            members.Add("s3");
            members.Add("s4");
            members.Add("fake1");
            members.Add("fake2");

            List<string> teamPrefList = new List<string>();
            teamPrefList.Add("s1");
            teamPrefList.Add("s2");
            teamPrefList.Add("s4");
            teamPrefList.Add("s3");
            teamPrefList.Add("fake1");
            teamPrefList.Add("fake2");

            Dictionary<string, List<string>> studensPrefList = new Dictionary<string, List<string>>();
            List<string> s1L = new List<string>();
            s1L.Add("t1");
            s1L.Add("t2");
            s1L.Add("t3");

            List<string> s2L = new List<string>();
            s2L.Add("t3");
            s2L.Add("t2");
            s2L.Add("t1");

            studensPrefList.Add("s1", s2L);
            studensPrefList.Add("s2", s1L);
            studensPrefList.Add("s3", s1L);
            studensPrefList.Add("s4", s1L);
            studensPrefList.Add("fake1", s2L);
            studensPrefList.Add("fake2", s1L);

            int teamSize = 2;


            List<string> refused = new List<string>();
            refused.Add("fake1");
            refused.Add("fake2");
            refused.Add("s3");

            List<string> f1Pref = new List<string>();
            f1Pref.Add("t3");
            f1Pref.Add("t2");
           
            List<string> f2Pref = new List<string>();
            f2Pref.Add("t2");
            f2Pref.Add("t3");

            List<string> s3Pref = new List<string>();
            s3Pref.Add("t2");
            s3Pref.Add("t3");

            refused.Should().BeEquivalentTo(controller.refuse(team, members, teamPrefList, studensPrefList, teamSize));

            studensPrefList["fake1"].Should().Equal(f1Pref);
            studensPrefList["fake2"].Should().Equal(f2Pref);
            studensPrefList["s3"].Should().Equal(s3Pref);
        }






        //public Dictionary<string, List<string>> initializeDict(Dictionary<string, List<string>> teamsPrefList)
        [Fact]
        public void initializeResult_Test()
        {
            Dictionary<string, List<string>> teamsPrefList = new Dictionary<string, List<string>>();
            List<string> tpref = new List<string>();
            List<string> tpref2 = new List<string>();
            List<string> tpref3 = new List<string>();
            List<string> tpref4 = new List<string>();
            List<string> tpref5 = new List<string>();

            tpref.Add("s3");
            tpref.Add("s5");
            tpref.Add("s2");
            tpref.Add("s1");
            tpref.Add("s4");

            tpref2.Add("s5");
            tpref2.Add("s2");
            tpref2.Add("s1");
            tpref2.Add("s4");
            tpref2.Add("s3");

            tpref3.Add("s4");
            tpref3.Add("s3");
            tpref3.Add("s5");
            tpref3.Add("s1");
            tpref3.Add("s2");

            tpref4.Add("s1");
            tpref4.Add("s2");
            tpref4.Add("s3");
            tpref4.Add("s4");
            tpref4.Add("s5");

            tpref5.Add("s2");
            tpref5.Add("s3");
            tpref5.Add("s4");
            tpref5.Add("s2");
            tpref5.Add("s5");

            teamsPrefList.Add("t1", tpref);
            teamsPrefList.Add("t2", tpref2);
            teamsPrefList.Add("t3", tpref3);
            teamsPrefList.Add("t4", tpref4);
            teamsPrefList.Add("t5", tpref5);

            Dictionary<string, List<string>> algo = new Dictionary<string, List<string>>();
            List<string> prefList = new List<string>();
            algo.Add("t1", prefList);
            algo.Add("t2", prefList);
            algo.Add("t3", prefList);
            algo.Add("t4", prefList);
            algo.Add("t5", prefList);

            algo.Should().BeEquivalentTo(controller.initializeResult(teamsPrefList));
        }


        //public List<string> initializeList(Dictionary<string, List<string>> studentsPerfList)
        [Fact]
        public void initializeNeedPlace_Test()
        {
            List<string> needPlace = new List<string>();

            needPlace.Add("s1");
            needPlace.Add("s2");
            needPlace.Add("s3");
            needPlace.Add("s4");
            needPlace.Add("s5");

            Dictionary<string, List<string>> studentsPrefList = new Dictionary<string, List<string>>();

            List<string> spref = new List<string>();
            List<string> spref2 = new List<string>();
            List<string> spref3 = new List<string>();
            List<string> spref4 = new List<string>();
            List<string> spref5 = new List<string>();

            spref.Add("t3");
            spref.Add("t2");
            spref.Add("t5");
            spref.Add("t1");
            spref.Add("t4");

            spref2.Add("t1");
            spref2.Add("t2");
            spref2.Add("t5");
            spref2.Add("t3");
            spref2.Add("t4");

            spref3.Add("t4");
            spref3.Add("t3");
            spref3.Add("t2");
            spref3.Add("t1");
            spref3.Add("t5");

            spref4.Add("t1");
            spref4.Add("t3");
            spref4.Add("t4");
            spref4.Add("t2");
            spref4.Add("t5");

            spref5.Add("t1");
            spref5.Add("t2");
            spref5.Add("t4");
            spref5.Add("t5");
            spref5.Add("t3");

            studentsPrefList.Add("s1", spref);
            studentsPrefList.Add("s2", spref2);
            studentsPrefList.Add("s3", spref3);
            studentsPrefList.Add("s4", spref4);
            studentsPrefList.Add("s5", spref5);

            needPlace.Should().BeEquivalentTo(controller.initializeNeedPlace(studentsPrefList));
        }







        //public void joinTeam(String student, List<String> prefList, Dictionary<string, List<string>> result)
        [Fact]
        public void joinTeam_Test()
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            List<string> t1L = new List<string>();
            t1L.Add("s1");
            t1L.Add("s2");
            result.Add("t1", t1L);
            List<string> t2L = new List<string>();
            t2L.Add("fake1");
            result.Add("t2", t2L);

            List<String> prefList = new List<string>();
            prefList.Add("t2");
            prefList.Add("t1");

            controller.joinTeam("s3", prefList, result);

            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>();
            List<string> e1L = new List<string>();
            e1L.Add("s1");
            e1L.Add("s2");
            expected.Add("t1", t1L);
            List<string> e2L = new List<string>();
            e2L.Add("fake1");
            e2L.Add("s3");
            expected.Add("t2", t2L);

            expected.Should().BeEquivalentTo(result);
        }







        //public void createFakeStudents(int teamSize, Dictionary<String, List<String>> studentsPerfList, Dictionary<String, List<String>> teamsPrefList)
        [Fact]
        public void createFakeStudents_moreStudentsNeed_Test()
        {
            Dictionary<string, List<string>> studentsPerfList = new Dictionary<string, List<string>>();
            List<string> spref = new List<string>();
            List<string> spref2 = new List<string>();
            List<string> spref3 = new List<string>();
            spref.Add("t1");
            spref2.Add("t1");
            spref.Add("t2");
            spref2.Add("t2");
            studentsPerfList.Add("s1", spref);
            studentsPerfList.Add("s2", spref2);
            spref3.Add("t2");
            spref3.Add("t1");
            studentsPerfList.Add("s3", spref3);

            Dictionary<string, List<string>> teamsPerfList = new Dictionary<string, List<string>>();
            List<string> tpref = new List<string>();
            List<string> tpref2 = new List<string>();
            tpref.Add("s1");
            tpref.Add("s2");
            tpref.Add("s3");
            teamsPerfList.Add("t1", tpref);
            tpref2.Add("s3");
            tpref2.Add("s2");
            tpref2.Add("s1");
            teamsPerfList.Add("t2", tpref2);

            controller.createFakeStudents(3, studentsPerfList, teamsPerfList);

            Dictionary<string, List<string>> expectedStudPref = new Dictionary<string, List<string>>();
            List<string> espref = new List<string>();
            List<string> espref2 = new List<string>();
            List<string> espref3 = new List<string>();
            List<string> espreff = new List<string>();
            List<string> espreff2 = new List<string>();
            List<string> espreff3 = new List<string>();
            espref.Add("t1");
            espref2.Add("t1");
            espref.Add("t2");
            espref2.Add("t2");
            expectedStudPref.Add("s1", espref);
            expectedStudPref.Add("s2", espref2);
            espref3.Add("t2");
            espref3.Add("t1");
            expectedStudPref.Add("s3", espref3);
            espreff.Add("t2");
            espreff.Add("t1");
            expectedStudPref.Add("fake1", espreff);
            espreff2.Add("t2");
            espreff2.Add("t1");
            expectedStudPref.Add("fake2", espreff2);
            espreff3.Add("t2");
            espreff3.Add("t1");
            expectedStudPref.Add("fake3", espreff3);

            expectedStudPref.Should().BeEquivalentTo(studentsPerfList);

            Dictionary<string, List<string>> expectedTeamsPerf= new Dictionary<string, List<string>>();
            List<string> etpref = new List<string>();
            List<string> etpref2 = new List<string>();
            etpref.Add("s1");
            etpref.Add("s2");
            etpref.Add("s3");
            etpref.Add("fake1");
            etpref.Add("fake2");
            etpref.Add("fake3");
            expectedTeamsPerf.Add("t1", etpref);
            etpref2.Add("s3");
            etpref2.Add("s2");
            etpref2.Add("s1");
            etpref2.Add("fake1");
            etpref2.Add("fake2");
            etpref2.Add("fake3");
            expectedTeamsPerf.Add("t2", etpref2);

            expectedTeamsPerf.Should().BeEquivalentTo(expectedTeamsPerf);
            etpref.Should().Equal(tpref);
            etpref2.Should().Equal(tpref2);
        }

        [Fact]
        public void createFakeStudents_noStudentsNeed_Test()
        {
            Dictionary<string, List<string>> studentsPerfList = new Dictionary<string, List<string>>();
            List<string> spref = new List<string>();
            List<string> spref2 = new List<string>();
            List<string> spref3 = new List<string>();
            List<string> spref4 = new List<string>();
            spref.Add("t1");
            spref2.Add("t1");
            spref.Add("t2");
            spref2.Add("t2");
            studentsPerfList.Add("s1", spref);
            studentsPerfList.Add("s2", spref2);
            spref3.Add("t2");
            spref3.Add("t1");
            studentsPerfList.Add("s3", spref3);
            spref4.Add("t2");
            spref4.Add("t1");
            studentsPerfList.Add("s4", spref4);

            Dictionary<string, List<string>> teamsPerfList = new Dictionary<string, List<string>>();
            List<string> tpref = new List<string>();
            List<string> tpref2 = new List<string>();
            tpref.Add("s1");
            tpref.Add("s2");
            tpref.Add("s3");
            tpref.Add("s4");
            teamsPerfList.Add("t1", tpref);
            tpref2.Add("s3");
            tpref2.Add("s2");
            tpref2.Add("s1");
            tpref2.Add("s4");
            teamsPerfList.Add("t2", tpref2);

            controller.createFakeStudents(2, studentsPerfList, teamsPerfList);

            Dictionary<string, List<string>> expectedStudPref = new Dictionary<string, List<string>>();
            List<string> espref = new List<string>();
            List<string> espref2 = new List<string>();
            List<string> espref3 = new List<string>();
            List<string> espref4 = new List<string>();
            espref.Add("t1");
            espref2.Add("t1");
            espref.Add("t2");
            espref2.Add("t2");
            expectedStudPref.Add("s1", espref);
            expectedStudPref.Add("s2", espref2);
            espref3.Add("t2");
            espref3.Add("t1");
            expectedStudPref.Add("s3", espref3);
            espref4.Add("t2");
            espref4.Add("t1");
            expectedStudPref.Add("s4", espref4);
            

            expectedStudPref.Should().BeEquivalentTo(studentsPerfList);

            Dictionary<string, List<string>> expectedTeamsPerf = new Dictionary<string, List<string>>();
            List<string> etpref = new List<string>();
            List<string> etpref2 = new List<string>();
            etpref.Add("s1");
            etpref.Add("s2");
            etpref.Add("s3");
            etpref.Add("s4");
            expectedTeamsPerf.Add("t1", etpref);
            etpref2.Add("s3");
            etpref2.Add("s2");
            etpref2.Add("s1");
            etpref2.Add("s4");
            expectedTeamsPerf.Add("t2", etpref2);

            expectedTeamsPerf.Should().BeEquivalentTo(expectedTeamsPerf);
        }





        //public bool hasFreePlace(int teamSize, Dictionary<String, List<String>> result)
        //if has free place
        [Fact]
        public void hasFreePlace_ifHas_Test()
        {
            Dictionary<String, List<String>> result = new Dictionary<string, List<string>>();
            List<string> t1 = new List<string>();
            t1.Add("s1");
            t1.Add("s2");
            t1.Add("s3");
            List<string> t2 = new List<string>();
            t2.Add("s4");
            t2.Add("s5");
            t2.Add("s6");
            List<string> t3 = new List<string>();
            t3.Add("s7");
            t3.Add("s8");
            result.Add("t1", t1);
            result.Add("t2", t2);
            result.Add("t3", t3);
            Assert.True(controller.hasFreePlace(3, result));
        }

        //is doesn't have free place
        [Fact]
        public void hasFreePlace_ifDoesNotHave_Test()
        {
            Dictionary<String, List<String>> result = new Dictionary<string, List<string>>();
            List<string> t1 = new List<string>();
            t1.Add("s1");
            t1.Add("s2");
            t1.Add("s3");
            List<string> t2 = new List<string>();
            t2.Add("s4");
            t2.Add("s5");
            t2.Add("s6");
            List<string> t3 = new List<string>();
            t3.Add("s7");
            t3.Add("s8");
            t3.Add("s9");
            result.Add("t1", t1);
            result.Add("t2", t2);
            result.Add("t3", t3);
            Assert.False(controller.hasFreePlace(3, result));
        }








        //public List<Dictionary<string, List<string>>> createPrefLists(Dictionary<string, Dictionary<string, List<string>>> studentsAnswers, Dictionary<string, Dictionary<string, List<string>>> teamsAnswers, List<int> weights, List<string> types)
        [Fact]
        public void createPrefLists_Test()
        {
            Dictionary<string, Dictionary<string, List<string>>> sAns = new Dictionary<string, Dictionary<string, List<string>>>();
            Dictionary<string, Dictionary<string, List<string>>> tAns = new Dictionary<string, Dictionary<string, List<string>>>();
                List<string> slist1 = new List<string>();
                slist1.Add("a");
                slist1.Add("b");
                slist1.Add("e");
                Dictionary<string, List<string>> sans = new Dictionary<string, List<string>>();
                sans.Add("1", slist1);
                List<string> slist2 = new List<string>();
                slist2.Add("TRUE");
                sans.Add("2", slist2);
                List<string> slist3 = new List<string>();
                slist3.Add("c");
                sans.Add("3", slist3);
                List<string> slist4 = new List<string>();
                slist4.Add("14");
                sans.Add("4", slist4);
                List<int> weights = new List<int>();
                weights.Add(10);
                weights.Add(30);
                weights.Add(5);
                weights.Add(15);
                sAns.Add("s1", sans);

                List<string> slist5 = new List<string>();
                slist5.Add("a");
                slist5.Add("b");
                slist5.Add("e");
                Dictionary<string, List<string>> sans2 = new Dictionary<string, List<string>>();
                sans2.Add("1", slist5);
                List<string> slist6 = new List<string>();
                slist6.Add("TRUE");
                sans2.Add("2", slist6);
                List<string> slist7 = new List<string>();
                slist7.Add("c");
                sans2.Add("3", slist7);
                List<string> slist8 = new List<string>();
                slist8.Add("14");
                sans2.Add("4", slist8);
                sAns.Add("s2", sans2);

                List<string> slist9 = new List<string>();
                slist9.Add("a");
                slist9.Add("b");
                slist9.Add("e");
                Dictionary<string, List<string>> sans3 = new Dictionary<string, List<string>>();
                sans3.Add("1", slist9);
                List<string> slist10 = new List<string>();
                slist10.Add("TRUE");
                sans3.Add("2", slist10);
                List<string> slist11 = new List<string>();
                slist11.Add("c");
                sans3.Add("3", slist11);
                List<string> slist12 = new List<string>();
                slist12.Add("14");
                sans3.Add("4", slist12);
                sAns.Add("s3", sans3);

                List<string> tlist1 = new List<string>();
                tlist1.Add("a");
                tlist1.Add("b");
                tlist1.Add("e");
                Dictionary<string, List<string>> tans1 = new Dictionary<string, List<string>>();
                tans1.Add("1", tlist1);
                List<string> tlist2 = new List<string>();
                tlist2.Add("TRUE");
                tans1.Add("2", tlist2);
                List<string> tlist3 = new List<string>();
                tlist3.Add("c");
                tans1.Add("3", tlist3);
                List<string> tlist4 = new List<string>();
                tlist4.Add("14");
                tans1.Add("4", tlist4);
                tAns.Add("t1", tans1);

                List<string> tlist5 = new List<string>();
                tlist5.Add("a");
                tlist5.Add("b");
                tlist5.Add("e");
                Dictionary<string, List<string>> tans2 = new Dictionary<string, List<string>>();
                tans2.Add("1", tlist5);
                List<string> tlist6 = new List<string>();
                tlist6.Add("TRUE");
                tans2.Add("2", tlist6);
                List<string> tlist7 = new List<string>();
                tlist7.Add("c");
                tans2.Add("3", tlist7);
                List<string> tlist8 = new List<string>();
                tlist8.Add("14");
                tans2.Add("4", tlist8);
                tAns.Add("t2", tans2);

                List<SkillType> types = new List<SkillType>();
                types.Add(SkillType.Checkbox);
                types.Add(SkillType.Dropdown);
                types.Add(SkillType.Checkbox);
                types.Add(SkillType.RadioButton);

            List<Dictionary<string, List<string>>> expected = new List<Dictionary<string, List<string>>>();
            Dictionary<string, List<string>> teams = new Dictionary<string, List<string>>();
            List<string> t1L = new List<string>();
            t1L.Add("s1");
            t1L.Add("s2");
            t1L.Add("s3");
            teams.Add("t1", t1L);
            List<string> t2L = new List<string>();
            t2L.Add("s1");
            t2L.Add("s2");
            t2L.Add("s3");
            teams.Add("t2", t2L);
            expected.Add(teams);

            Dictionary<string, List<string>> students = new Dictionary<string, List<string>>();
            List<string> s1L = new List<string>();
            s1L.Add("t1");
            s1L.Add("t2");
            students.Add("s1", s1L);
            List<string> s2L = new List<string>();
            s2L.Add("t1");
            s2L.Add("t2");
            students.Add("s2", s2L);
            List<string> s3L = new List<string>();
            s3L.Add("t1");
            s3L.Add("t2");
            students.Add("s3", s3L);
            expected.Add(students);


            expected.Should().BeEquivalentTo(controller.createPrefLists(sAns, tAns, weights, types));
        }









        //public List<string> makeSortedList(List<int> points, List<string> possibilities)
        [Fact]
        public void makeSortedList_Test()
        {
            List<int> points = new List<int>();
            points.Add(12);
            points.Add(23);
            points.Add(1);
            points.Add(8);
            points.Add(15);
            points.Add(0);
            List<string> possiblities = new List<string>();
            possiblities.Add("t1");
            possiblities.Add("t2");
            possiblities.Add("t3");
            possiblities.Add("t4");
            possiblities.Add("t5");
            possiblities.Add("t6");


            List<string> expected = new List<string>();
            expected.Add("t2");
            expected.Add("t5");
            expected.Add("t1");
            expected.Add("t4");
            expected.Add("t3");
            expected.Add("t6");

            expected.Should().Equal(controller.makeSortedList(points, possiblities));
        }







        // public int calculate([FromQuery] KeyValuePair<string, Dictionary<string, List<string>>> s, [FromQuery] KeyValuePair<string, Dictionary<string, List<string>>> t, [FromQuery] List<int> weights, [FromQuery] List<string> types)
        [Fact]
        public void calculate_complex_Test()
        {
            int expected = 65;

            Dictionary<string, Dictionary<string, List<string>>> s = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> sAns = new List<string>();
            sAns.Add("a");
            sAns.Add("c");
            List<string> sAns2 = new List<string>();
            sAns2.Add("a");
            List<string> sAns3 = new List<string>();
            sAns3.Add("b");
            Dictionary<string, List<string>> sQuest = new Dictionary<string, List<string>>();
            sQuest.Add("q1", sAns);
            sQuest.Add("q2", sAns2);
            sQuest.Add("q3", sAns3);
            s.Add("s1", sQuest);
            Dictionary<string, Dictionary<string, List<string>>> t = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> tAns = new List<string>();
            tAns.Add("a");
            tAns.Add("b");
            List<string> tAns2 = new List<string>();
            tAns2.Add("a");
            List<string> tAns3 = new List<string>();
            tAns3.Add("c");
            Dictionary<string, List<string>> tQuest = new Dictionary<string, List<string>>();
            tQuest.Add("q1", tAns);
            tQuest.Add("q2", tAns2);
            tQuest.Add("q3", tAns3);
            t.Add("t1", tQuest);
            List<SkillType> types = new List<SkillType>();
            types.Add(SkillType.Checkbox);
            types.Add(SkillType.Dropdown);
            types.Add(SkillType.RadioButton);
            List<int> weights = new List<int>();
            weights.Add(50);
            weights.Add(40);
            weights.Add(10);

            Assert.True(expected == controller.calculate(s.ElementAt(0), t.ElementAt(0), weights, types));
        }

        [Fact]
        public void calculate_Checkbox_Test()
        {
            int expected = 10;

            Dictionary<string, Dictionary<string, List<string>>> s = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> sAns = new List<string>();
            sAns.Add("a");
            sAns.Add("c");
            Dictionary<string, List<string>> sQuest = new Dictionary<string, List<string>>();
            sQuest.Add("q1", sAns);
            s.Add("s1", sQuest);
            Dictionary<string, Dictionary<string, List<string>>> t = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> tAns = new List<string>();
            tAns.Add("a");
            tAns.Add("c");
            Dictionary<string, List<string>> tQuest = new Dictionary<string, List<string>>();
            tQuest.Add("q1", tAns);
            t.Add("t1", tQuest);
            List<SkillType> types = new List<SkillType>();
            types.Add(SkillType.Checkbox);
            List<int> weights = new List<int>();
            weights.Add(10);

            Assert.True(expected == controller.calculate(s.ElementAt(0), t.ElementAt(0), weights, types));
        }

        [Fact]
        public void calculate_Dropdown_Test()
        {
            int expected = 12;

            Dictionary<string, Dictionary<string, List<string>>> s = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> sAns = new List<string>();
            sAns.Add("a");
            Dictionary<string, List<string>> sQuest = new Dictionary<string, List<string>>();
            sQuest.Add("q1", sAns);
            s.Add("s1", sQuest);
            Dictionary<string, Dictionary<string, List<string>>> t = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> tAns = new List<string>();
            tAns.Add("a");
            Dictionary<string, List<string>> tQuest = new Dictionary<string, List<string>>();
            tQuest.Add("q1", tAns);
            t.Add("t1", tQuest);
            List<SkillType> types = new List<SkillType>();
            types.Add(SkillType.Dropdown);
            List<int> weights = new List<int>();
            weights.Add(12);

            Assert.True(expected == controller.calculate(s.ElementAt(0), t.ElementAt(0), weights, types));
        }

        [Fact]
        public void calculate_RadioButton_Test()
        {
            int expected = 14;

            Dictionary<string, Dictionary<string, List<string>>> s = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> sAns = new List<string>();
            sAns.Add("a");
            Dictionary<string, List<string>> sQuest = new Dictionary<string, List<string>>();
            sQuest.Add("q1", sAns);
            s.Add("s1", sQuest);
            Dictionary<string, Dictionary<string, List<string>>> t = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> tAns = new List<string>();
            tAns.Add("a");
            Dictionary<string, List<string>> tQuest = new Dictionary<string, List<string>>();
            tQuest.Add("q1", tAns);
            t.Add("t1", tQuest);
            List<SkillType> types = new List<SkillType>();
            types.Add(SkillType.RadioButton);
            List<int> weights = new List<int>();
            weights.Add(14);

            Assert.True(expected==controller.calculate(s.ElementAt(0), t.ElementAt(0), weights, types));
        }







        //public int calculateCheckbox(List<string> sAns, List<string> tAns, int weight)
        [Fact]
        public void calculateCheckbox_maxpoint_Test()
        {
            int expected = 10;

            List<string> sAns = new List<string>();
            sAns.Add("a");
            sAns.Add("c");

            List<string> tAns = new List<string>();
            tAns.Add("a");
            tAns.Add("c");

            Assert.True(expected == controller.calculateCheckbox(sAns, tAns, 10));
        }

        [Fact]
        public void calculateCheckbox_partialpoint_Test()
        {
            int expected = 5;

            List<string> sAns = new List<string>();
            sAns.Add("a");
            sAns.Add("c");

            List<string> tAns = new List<string>();
            tAns.Add("a");
            tAns.Add("d");

            Assert.True(expected == controller.calculateCheckbox(sAns, tAns, 10));
        }

        [Fact]
        public void calculateCheckbox_zeropoint_Test()
        {
            int expected = 0;

            List<string> sAns = new List<string>();
            sAns.Add("a");
            sAns.Add("c");

            List<string> tAns = new List<string>();
            tAns.Add("b");
            tAns.Add("d");

            Assert.True(expected == controller.calculateCheckbox(sAns, tAns, 10));
        }






        //public int calculateDropdown(List<string> sAns, List<string> tAns, int weight)
        [Fact]
        public void calculateDropdown_maxPoint_Test()
        {
            int expected = 50;

            List<string> sAns = new List<string>();
            sAns.Add("a");

            List<string> tAns = new List<string>();
            tAns.Add("a");

            Assert.True(expected == controller.calculateDropdown(sAns, tAns, 50));
        }

        [Fact]
        public void calculateDropdown_zeroPoint_Test()
        {
            int expected = 0;

            List<string> sAns = new List<string>();
            sAns.Add("a");

            List<string> tAns = new List<string>();
            tAns.Add("c");

            Assert.True(expected == controller.calculateDropdown(sAns, tAns, 50));
        }







        //public int calculateRadioButton(List<string> sAns, List<string> tAns, int weight)
        [Fact]
        public void calculateRadioButton_maxPoint_Test()
        {
            int expected = 50;

            List<string> sAns = new List<string>();
            sAns.Add("a");

            List<string> tAns = new List<string>();
            tAns.Add("a");

            Assert.True(expected == controller.calculateRadioButton(sAns, tAns, 50));
        }

        [Fact]
        public void calculateRadioButton_zeroPoint_Test()
        {
            int expected = 0;

            List<string> sAns = new List<string>();
            sAns.Add("a");

            List<string> tAns = new List<string>();
            tAns.Add("b");

            Assert.True(expected == controller.calculateRadioButton(sAns, tAns, 50));
        }

    }
}
